package digidigi.mtmechs.sound;

import digidigi.mtmechs.entity.TunnelArmorEntity;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.network.OtherClientPlayerEntity;
import net.minecraft.client.sound.MovingSoundInstance;
import net.minecraft.client.sound.SoundManager;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.math.MathHelper;

@Environment(EnvType.CLIENT)
public class DrillingSoundInstance extends MovingSoundInstance {

	public TunnelArmorEntity parent_entity;
	public float distance = 0.0F;
	public SoundManager soundManager;
	public boolean finished = false;
	public float defaultVolume;

	public DrillingSoundInstance(TunnelArmorEntity taentity, SoundEvent sound, SoundCategory category, float volume,
			float pitch, double x, double y, double z) {
		super(sound, category);
		this.parent_entity = taentity;
		this.repeat = true;
		this.repeatDelay = 0;
		this.volume = volume;
		this.defaultVolume = volume;
		this.pitch = pitch;
		this.x = x;
		this.y = y;
		this.z = z;
		this.soundManager = MinecraftClient.getInstance().getSoundManager();
	}

	public boolean canPlay() {
		return !this.isDone();
	}

	public boolean shouldAlwaysPlay() {
		return true;
	}
	
	public void moveSound() {
		this.x = (double) ((float) this.parent_entity.getX());
		this.y = (double) ((float) this.parent_entity.getY());
		this.z = (double) ((float) this.parent_entity.getZ());
	}

	@Override
	public void tick() {
		if (parent_entity.getFirstPassenger() instanceof OtherClientPlayerEntity) {
			ClientPlayerEntity player = MinecraftClient.getInstance().player;
			OtherClientPlayerEntity otherplayer = (OtherClientPlayerEntity) parent_entity.getFirstPassenger();
			double distancePre = Math.sqrt(Math.pow(player.getX() - otherplayer.getX(), 2) + Math.pow(player.getZ() - otherplayer.getZ(), 2));
			distancePre *= 100;
			double volDistance = 1.0F + distancePre;
			this.volume = (float) (this.defaultVolume / volDistance);
		}
		if (this.finished) {
			this.soundManager.stop(this);
			
		}
		else {
			if (this.isDone()) {
				this.soundManager.playNextTick(this);
			}
			else {
				this.x = (double) ((float) this.parent_entity.getX());
				this.y = (double) ((float) this.parent_entity.getY());
				this.z = (double) ((float) this.parent_entity.getZ());
				this.distance = MathHelper.clamp(this.distance + 0.0025F, 0.0F, 1.0F);
			}
		}

	}
	
	@Override
	public boolean isRepeatable() {
		return true;
	}

	@Override
	public boolean isRelative() {
		return false;
	}

}
