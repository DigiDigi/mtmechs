package digidigi.mtmechs.sound;

import digidigi.mtmechs.entity.ProtoArmorEntity;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.sound.MovingSoundInstance;
import net.minecraft.client.sound.SoundManager;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvent;
import net.minecraft.util.math.MathHelper;

@Environment(EnvType.CLIENT)
public class PAStepSoundInstance extends MovingSoundInstance {

	public ProtoArmorEntity parent_entity;
	public float distance = 0.0F;
	public SoundManager soundManager;

	public PAStepSoundInstance(ProtoArmorEntity paentity, SoundEvent sound, SoundCategory category, float volume,
			float pitch, double x, double y, double z) {
		super(sound, category);
		this.parent_entity = paentity;
		this.repeat = false;
		this.repeatDelay = 0;
		this.volume = volume;
		this.pitch = pitch;
		this.x = x;
		this.y = y;
		this.z = z;
		this.soundManager = MinecraftClient.getInstance().getSoundManager();
	}

	public boolean canPlay() {
		return !this.isDone();
	}

	public boolean shouldAlwaysPlay() {
		return false;
	}
	
	public void moveSound() {
		this.x = (double) ((float) this.parent_entity.getX());
		this.y = (double) ((float) this.parent_entity.getY());
		this.z = (double) ((float) this.parent_entity.getZ());
	}

	@Override
	public void tick() {
		if (this.isDone()) {
			this.soundManager.stop(this);
		}
		else {
			this.x = (double) ((float) this.parent_entity.getX());
			this.y = (double) ((float) this.parent_entity.getY());
			this.z = (double) ((float) this.parent_entity.getZ());
			
			this.distance = MathHelper.clamp(this.distance + 0.0025F, 0.0F, 1.0F);
			this.volume = MathHelper.lerp(MathHelper.clamp(1, 0.0F, 0.5F), 0.0F, 0.7F);
		}
	}
	
	@Override
	public boolean isRepeatable() {
		return false;
	}

	@Override
	public boolean isRelative() {
		return false;
	}

}
