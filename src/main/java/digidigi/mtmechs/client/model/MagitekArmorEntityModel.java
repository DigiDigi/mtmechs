package digidigi.mtmechs.client.model;

import digidigi.mtmechs.entity.MagitekArmorEntity;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.model.ModelPart;
import net.minecraft.entity.Entity;
import net.minecraft.util.Identifier;
import software.bernie.geckolib3.core.event.predicate.AnimationEvent;
import software.bernie.geckolib3.core.processor.IBone;
import software.bernie.geckolib3.model.AnimatedGeoModel;

public class MagitekArmorEntityModel extends AnimatedGeoModel<MagitekArmorEntity> {
	
    @Override
    public Identifier getModelLocation(MagitekArmorEntity object)
    {
        return new Identifier("mtmechs", "geo/magitekarmor.geo.json");
    }
    @Override
    public Identifier getTextureLocation(MagitekArmorEntity object)
    {
        return new Identifier("mtmechs", "textures/entity/magitekarmor.png");
    }
    @Override
    public Identifier getAnimationFileLocation(MagitekArmorEntity object)
    {
        return new Identifier("mtmechs", "animations/magitekarmor.animation.json");
    }
	
    public void setRotationAngles(ModelPart model, float x, float y, float z) {
    	// replaces SetRotation
    	model.pitch = x;
    	model.yaw = y;
    	model.roll = z;
    }
	@Override
	public void setLivingAnimations(MagitekArmorEntity entity, Integer uniqueID, AnimationEvent customPredicate) {
		super.setLivingAnimations(entity, uniqueID, customPredicate);
		if (entity.world.isClient) {
			this.tilt(entity);
		}
	}
	
	@Environment(EnvType.CLIENT)
	public void tilt(MagitekArmorEntity entity) {
		if (entity.hasPassengers()) {
			IBone axle = getAnimationProcessor().getBone("axle");
			Entity pilot = entity.getPassengerList().get(0);
			float pitchConstrained = Math.max(Math.min(pilot.getPitch(0), 10.0F), -10.0F);
			axle.setRotationZ(-( pitchConstrained * ((float) Math.PI / 180F)));
		}
	}
}
