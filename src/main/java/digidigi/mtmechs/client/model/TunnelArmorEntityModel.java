package digidigi.mtmechs.client.model;

import digidigi.mtmechs.entity.TunnelArmorEntity;
import net.minecraft.client.model.ModelPart;
import net.minecraft.util.Identifier;
import software.bernie.geckolib3.model.AnimatedGeoModel;

public class TunnelArmorEntityModel extends AnimatedGeoModel<TunnelArmorEntity> {
	
    @Override
    public Identifier getModelLocation(TunnelArmorEntity object)
    {
        return new Identifier("mtmechs", "geo/tunnelarmor.geo.json");
    }
    @Override
    public Identifier getTextureLocation(TunnelArmorEntity object)
    {
        return new Identifier("mtmechs", "textures/entity/tunnelarmor.png");
    }
    @Override
    public Identifier getAnimationFileLocation(TunnelArmorEntity object)
    {
        return new Identifier("mtmechs", "animations/tunnelarmor.animation.json");
    }
    
    public void setRotationAngles(ModelPart model, float x, float y, float z) {
    	// replaces SetRotation
    	model.pitch = x;
    	model.yaw = y;
    	model.roll = z;
    }


}