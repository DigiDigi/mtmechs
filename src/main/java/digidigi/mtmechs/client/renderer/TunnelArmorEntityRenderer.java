package digidigi.mtmechs.client.renderer;

import digidigi.mtmechs.client.model.TunnelArmorEntityModel;
import digidigi.mtmechs.entity.TunnelArmorEntity;
import digidigi.mtmechs.entity.feature.TunnelArmorCoreFeatureRenderer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.entity.EntityRendererFactory;
import software.bernie.geckolib3.renderers.geo.GeoEntityRenderer;

@Environment(EnvType.CLIENT)
public class TunnelArmorEntityRenderer extends GeoEntityRenderer<TunnelArmorEntity> {
	
    public TunnelArmorEntityRenderer(EntityRendererFactory.Context renderManager) {
        super(renderManager, new TunnelArmorEntityModel());
        this.addLayer(new TunnelArmorCoreFeatureRenderer(this, new TunnelArmorEntityModel()));
    }
    
	// Disable visible label.
    @Override
    public boolean hasLabel(TunnelArmorEntity entity) {
    	return false;
    }
    
}