package digidigi.mtmechs.client.renderer;

import digidigi.mtmechs.client.model.ProtoArmorEntityModel;
import digidigi.mtmechs.entity.ProtoArmorEntity;
import digidigi.mtmechs.entity.feature.ProtoArmorBeamFeatureRenderer;
import digidigi.mtmechs.entity.feature.ProtoArmorCoreFeatureRenderer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.entity.EntityRendererFactory;
import software.bernie.geckolib3.renderers.geo.GeoEntityRenderer;

@Environment(EnvType.CLIENT)
public class ProtoArmorEntityRenderer extends GeoEntityRenderer<ProtoArmorEntity> {
	
    public ProtoArmorEntityRenderer(EntityRendererFactory.Context renderManager) {
        super(renderManager, new ProtoArmorEntityModel());
        this.addLayer(new ProtoArmorCoreFeatureRenderer(this, new ProtoArmorEntityModel()));
        this.addLayer(new ProtoArmorBeamFeatureRenderer(this, new ProtoArmorEntityModel()));
    }

	// Disable visible label.
    @Override
    public boolean hasLabel(ProtoArmorEntity entity) {
    	return false;
    }
    
}