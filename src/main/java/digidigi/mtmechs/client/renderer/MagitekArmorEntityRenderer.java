package digidigi.mtmechs.client.renderer;

import digidigi.mtmechs.client.model.MagitekArmorEntityModel;
import digidigi.mtmechs.entity.MagitekArmorEntity;
import digidigi.mtmechs.entity.feature.MagitekArmorBeamFeatureRenderer;
import digidigi.mtmechs.entity.feature.MagitekArmorCoreFeatureRenderer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.entity.EntityRendererFactory;
import software.bernie.geckolib3.renderers.geo.GeoEntityRenderer;

@Environment(EnvType.CLIENT)
public class MagitekArmorEntityRenderer extends GeoEntityRenderer<MagitekArmorEntity> {

    public MagitekArmorEntityRenderer(EntityRendererFactory.Context renderManager) {
        super(renderManager, new MagitekArmorEntityModel());
        this.addLayer(new MagitekArmorCoreFeatureRenderer(this, new MagitekArmorEntityModel()));
        this.addLayer(new MagitekArmorBeamFeatureRenderer(this, new MagitekArmorEntityModel()));
    }

	// Disable visible label.
    @Override
    public boolean hasLabel(MagitekArmorEntity entity) {
    	return false;
    }

}