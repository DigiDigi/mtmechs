package digidigi.mtmechs.compat;

import draylar.goml.api.ClaimUtils;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.stream.Collectors;

public class GOMLCompat {
    public static boolean installed = false;
    public static boolean hasPermission(World world, BlockPos blockPos, ServerPlayerEntity player) {
        if (installed) {
            if (ClaimUtils.getClaimsAt(world, blockPos).collect(Collectors.toList()).stream().allMatch(entry -> entry.getValue().hasPermission(player))) {
                return true;
            } else {
                return false;
            }
        }
        return true;
    }
}
