package digidigi.mtmechs.compat;

import io.github.flemmli97.flan.api.ClaimHandler;
import io.github.flemmli97.flan.api.permission.PermissionRegistry;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class FlanCompat {
    public static boolean installed = false;
    public static boolean hasPermission(World world, BlockPos blockPos, ServerPlayerEntity player) {
        if (installed) {
            if (ClaimHandler.getPermissionStorage((ServerWorld) world).getForPermissionCheck(blockPos).canInteract(player, PermissionRegistry.PLACE, blockPos)) {
                {
                    return true;
                }
            } else {
                return false;
            }
        }
        return true;
    }
}
