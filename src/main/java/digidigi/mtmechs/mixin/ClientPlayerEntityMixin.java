package digidigi.mtmechs.mixin;

import net.minecraft.client.MinecraftClient;
import net.minecraft.client.input.Input;
import net.minecraft.client.network.AbstractClientPlayerEntity;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.recipebook.ClientRecipeBook;
import net.minecraft.client.world.ClientWorld;
import net.minecraft.stat.StatHandler;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import digidigi.mtmechs.entity.MagitekArmorEntity;

@Mixin(ClientPlayerEntity.class)
public abstract class ClientPlayerEntityMixin extends AbstractClientPlayerEntity{
	
	public Input input;
	
	public ClientPlayerEntityMixin(MinecraftClient client, ClientWorld world, ClientPlayNetworkHandler networkHandler, StatHandler stats, ClientRecipeBook recipeBook, boolean lastSneaking, boolean lastSprinting) {
		super(world, networkHandler.getProfile());
	}

	@Inject(at = @At("TAIL"), method = "tickRiding()V")
	private void tickRidingCall(CallbackInfo info) {
		if (this.getVehicle() instanceof MagitekArmorEntity) {
			MagitekArmorEntity magitekarmorEntity = (MagitekArmorEntity)this.getVehicle();
		}
	}
}
