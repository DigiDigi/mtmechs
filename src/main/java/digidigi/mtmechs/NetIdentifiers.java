package digidigi.mtmechs;

import net.minecraft.util.Identifier;

public class NetIdentifiers {
    public static final Identifier ta_p_to_server = new Identifier("mtmechs", "ta-p-to-server");
    public static final Identifier ta_p_to_clients = new Identifier("mtmechs", "ta-p-to-clients");
    public static final Identifier ma_p_to_server = new Identifier("mtmechs", "ma-p-to-server");
    public static final Identifier ma_p_to_clients = new Identifier("mtmechs", "ma-p-to-clients");
    public static final Identifier pa_p_to_server = new Identifier("mtmechs", "pa-p-to-server");
    public static final Identifier pa_p_to_clients = new Identifier("mtmechs", "pa-p-to-clients");
    public static final Identifier ta_dismount_to_clients = new Identifier("mtmechs", "ta-dismount-to-clients");
    
}
