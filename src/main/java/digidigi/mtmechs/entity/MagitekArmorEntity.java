package digidigi.mtmechs.entity;

import java.util.Collection;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Predicate;

import digidigi.mtmechs.compat.FlanCompat;
import digidigi.mtmechs.compat.GOMLCompat;
import digidigi.mtmechs.sound.*;
import net.minecraft.client.network.OtherClientPlayerEntity;
import net.minecraft.entity.MovementType;
import net.minecraft.util.registry.Registry;
import org.jetbrains.annotations.Nullable;

import digidigi.mtmechs.MagitekMechs;
import digidigi.mtmechs.NetIdentifiers;
import digidigi.mtmechs.ServerEntityHitResult;
import digidigi.mtmechs.screen.MagitekArmorScreenHandler;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.fabricmc.fabric.api.networking.v1.PlayerLookup;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.fabric.api.registry.FuelRegistry;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.Material;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.client.network.ClientPlayerEntity;
import net.minecraft.client.sound.PositionedSoundInstance;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.mob.PathAwareEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtList;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.sound.SoundCategory;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.RaycastContext;
import net.minecraft.world.RaycastContext.FluidHandling;
import net.minecraft.world.RaycastContext.ShapeType;
import net.minecraft.world.World;
import software.bernie.geckolib3.core.AnimationState;
import software.bernie.geckolib3.core.IAnimatable;
import software.bernie.geckolib3.core.PlayState;
import software.bernie.geckolib3.core.builder.AnimationBuilder;
import software.bernie.geckolib3.core.controller.AnimationController;
import software.bernie.geckolib3.core.event.SoundKeyframeEvent;
import software.bernie.geckolib3.core.event.predicate.AnimationEvent;
import software.bernie.geckolib3.core.manager.AnimationData;
import software.bernie.geckolib3.core.manager.AnimationFactory;

public class MagitekArmorEntity extends PathAwareEntity implements IAnimatable {

    private AnimationFactory factory = new AnimationFactory(this);

    // PropertyDelegates use these when inventory screens are open.
    public int syncedMagiciteSlotted = -1;        // Magicite exists in Magicite slot
    public int syncedFuelSlotted = 0;            // 1 if fuel existing in slot(s)
    public int syncedEnergyStored = 0;            // Amount of energy remaining in reserve tank (or ongoing burn).
    public int syncedAddonSlotted = 0;            // Valid Addon exists in Addon slot.

    public int energyStored = 0;                // Actual energy value stored.
    public int energyMaximum = 0;                // Burned fuel sets its own cap.

    public boolean isOn = false;

    public BlockPos smeltBlockPos;
    public int smeltProgress;

    public SimpleInventory inventory;

    public int smokecounter = 0;

    // Armor step related flags for animation.
    public boolean animStepFirstComplete = false;
    public boolean animStepFirstOngoing = false;
    public boolean animNowWalking = false;

    // Beam related timer.
    public long deltaGathered = 0;
    public long deltaTime = 0;
    public long lastTime = 0;
    public long currentTime = 0;
    public long beamGraceTime = 0; // Limit the frequency of beam ticks by a set time.

    // Extra timer for armor steps when they leave the view and lose animation timing.
    public long stepCurrentTime = 0;
    public long stepDeltaTime = 0;
    public long stepLastTime = 0;
    public long stepDeltaGathered = 0;

    public long stepThreshold = 375;
    public boolean stepFirstCompleted = false;

    public static TrackedData<Byte> MA_FLAGS;
    public static TrackedData<Integer> ENERGY_STORED;
    public static TrackedData<Boolean> ALLOW_BEAM;
    public static TrackedData<Optional<UUID>> OWNER;

    public boolean beamNew = false;
    public boolean beamFiring = false;
    public boolean beamFiringEstablished = false;
    public boolean resetBeamRender = false;

    public int beamType = 0;

    public MAStepSoundInstance stepsound;
    public MAFirstStepSoundInstance firststepsound;
    public MABeamSoundInstance beamsound;
    public PositionedSoundInstance opening;

    static {
        MA_FLAGS = DataTracker.registerData(MagitekArmorEntity.class, TrackedDataHandlerRegistry.BYTE);
        ENERGY_STORED = DataTracker.registerData(MagitekArmorEntity.class, TrackedDataHandlerRegistry.INTEGER);
        ALLOW_BEAM = DataTracker.registerData(MagitekArmorEntity.class, TrackedDataHandlerRegistry.BOOLEAN);
        OWNER = DataTracker.registerData(MagitekArmorEntity.class, TrackedDataHandlerRegistry.OPTIONAL_UUID);
    }

    protected void initDataTracker() {
        super.initDataTracker();
        this.dataTracker.startTracking(MA_FLAGS, (byte) 0);
        this.dataTracker.startTracking(ENERGY_STORED, 0);
        this.dataTracker.startTracking(ALLOW_BEAM, true);
        this.dataTracker.startTracking(OWNER, Optional.empty());
    }

    public void dataSetIsOn(boolean truth) {
        if (this.isOn != truth) {
            //Get existing byte flag.
            byte b = (Byte) this.dataTracker.get(MA_FLAGS);
            //Add 'isOn' to the byte flags.
            this.dataTracker.set(MA_FLAGS, truth ? (byte) (b | 1) : (byte) (b & -2));
            //Set it locally. (This plus the conditional will also prevent repeats from the inventory listener.)
            this.isOn = truth;
        }
        // add bit to byte flags (or:) remove bit from byte flags
        // &-2 removes '1'
        // &-3 removes '2'
        // &-5 removes '4'
        // &-9 removes '8'
    }

    public boolean dataGetIsOn() {
        return ((Byte) this.dataTracker.get(MA_FLAGS) & 1) != 0;
    }

    public void dataSetBeamNew(boolean truth) {
        byte b = (Byte) this.dataTracker.get(MA_FLAGS);
        this.dataTracker.set(MA_FLAGS, truth ? (byte) (b | 2) : (byte) (b & -3));
    }

    public boolean dataGetBeamNew() {
        return ((Byte) this.dataTracker.get(MA_FLAGS) & 2) != 0;
    }

    public void dataSetBeamAllowed(boolean truth) {
        this.dataTracker.set(ALLOW_BEAM, truth);
    }

    public boolean dataGetBeamAllowed() {
        return ((Boolean) this.dataTracker.get(ALLOW_BEAM));
    }

    public void dataSetFuel(int energy) {
        this.dataTracker.set(ENERGY_STORED, energy);
    }

    public int dataGetFuel() {
        return this.dataTracker.get(ENERGY_STORED);
    }

    public void dataSetOwner(UUID owner) {
        this.dataTracker.set(OWNER, Optional.of(owner));
    }

    ;

    public Optional<UUID> dataGetOwner() {
        return this.dataTracker.get(OWNER);
    }

    public boolean hasSidewaysVelocity() {
        if (this.getVelocity().x != 0.0 | this.getVelocity().z != 0.0) {
            return true;
        } else {
            return false;
        }
    }

    private <E extends IAnimatable> PlayState predicate(AnimationEvent<E> event) {
        if (this.hasSidewaysVelocity() && !this.getPassengerList().isEmpty() && this.isOn && this.energyStored > 0) {
            if (animStepFirstComplete == false) {
                if (animStepFirstOngoing == false) {
                    event.getController().setAnimation(new AnimationBuilder().addAnimation("animation.magitekarmor.leftstep", false));
                    animStepFirstOngoing = true;
                } else {
                    if (event.getController().getAnimationState() == AnimationState.Stopped) {
                        animStepFirstOngoing = false;
                        animStepFirstComplete = true;
                    }
                }

            } else {
                if (animNowWalking == false) {
                    event.getController().setAnimation(new AnimationBuilder().addAnimation("animation.magitekarmor.leftcycle", true));
                    animNowWalking = true;
                }

            }
            return PlayState.CONTINUE;
        } else {
            event.getController().setAnimation(new AnimationBuilder().addAnimation("animation.magitekarmor.leftstep", false).addAnimation("animation.magitekarmor.leftcycle", false).addAnimation("animation.magitekarmor.default", true));
            animNowWalking = false;
            animStepFirstComplete = false;
            animStepFirstOngoing = false;
            return PlayState.STOP;
        }
    }

    @Override
    public void registerControllers(AnimationData data) {
        AnimationController controller = new AnimationController(this, "controller", 0, this::predicate);
        controller.registerSoundListener(this::soundListener);
        data.addAnimationController(controller);
    }

    private <ENTITY extends IAnimatable> void soundListener(SoundKeyframeEvent<ENTITY> event) {
        if (this.world.isClient) {
            if (this.hasPassengers()) {
                if (this.getFirstPassenger() instanceof OtherClientPlayerEntity) {
                    ClientPlayerEntity player = MinecraftClient.getInstance().player;
                    OtherClientPlayerEntity otherplayer = (OtherClientPlayerEntity) this.getFirstPassenger();
                    double distancePre = Math.sqrt(Math.pow(player.getX() - otherplayer.getX(), 2) + Math.pow(player.getZ() - otherplayer.getZ(), 2));
                    distancePre *= 1;
                    double distance = 1.0F + distancePre;

                    if (event.sound.contentEquals("mechstep")) {
                        this.makeFirstStepSound(this.getX(), this.getY(), distance);
                    } else if (event.sound.contentEquals("mechsteptwo")) {
                        this.makeStepSound(this.getX(), this.getY(), distance);
                    }

                } else {
                    ClientPlayerEntity pilot = (ClientPlayerEntity) this.getFirstPassenger();
                    if (event.sound.contentEquals("mechstep")) {
                        pilot.playSound(MagitekMechs.mechstep, 1, 1);
                    } else if (event.sound.contentEquals("mechsteptwo")) {
                        pilot.playSound(MagitekMechs.mechsteptwo, 3, 1);
                    }
                }
            }
        }
    }

    @Override
    public AnimationFactory getFactory() {
        return this.factory;
    }

    public MagitekArmorEntity(EntityType<? extends PathAwareEntity> entityType, World world) {
        super(entityType, world);
        this.stepHeight = 1.0F;
        this.setYaw(0F);
        this.setSneaking(true);
        this.inventory = new SimpleInventory(12);
        this.ignoreCameraFrustum = true;
    }

    @Nullable
    public Entity getControllingPassenger() {
        return this.getPassengerList().isEmpty() ? null : this.getPassengerList().get(0);
    }

    // Only for this Armor type
    public void tickFuel() {
        if (!this.world.isClient) {
            int spEnergyStored = 0;
            if (energyStored == 0) {
                if (this.syncedMagiciteSlotted == 1) {
                    ItemStack fuelStack = this.inventory.getStack(9);
                    if (fuelStack != null) {
                        Item fuelItem = fuelStack.getItem();
                        if (fuelItem != null) {
                            if (FuelRegistry.INSTANCE.get(fuelItem) != null) {
                                // Here the item is fuel and it has a value.
                                int fuelAmount = FuelRegistry.INSTANCE.get(fuelItem);
                                // Remove and expend the fuel.
                                this.inventory.removeStack(9, 1);
                                energyStored += fuelAmount;
                                energyMaximum = fuelAmount;
                            }
                        }
                    }
                }
            } else {
                energyStored -= 1;
                if (energyStored < 0) {
                    energyStored = 0;
                }
                float dv = (float) (energyMaximum / 10.0);
                spEnergyStored = (int) Math.ceil(energyStored / dv);
            }
            this.propertyDelegate.set(2, spEnergyStored);
            this.dataSetFuel(this.energyStored);
        }
    }

    @Environment(EnvType.CLIENT)
    public void tickSmoke() {
        if (this.world.isClient && this.energyStored > 0) {
            this.smokecounter += 1;
            if (this.smokecounter == 7) {
                this.smokecounter = 0;
            }
            if (this.smokecounter == 0) {
                Vec3d p = this.getPos();
                double aim = Math.toRadians(this.getYaw() * -1);
                // forward vector to add
                double fx = Math.sin(aim);
                double fz = Math.cos(aim);
                // leftward side vector to add or subtract
                double sx = fz;
                double sz = fx * -1;

                if (this.isOn) {
                    Vec3d l = new Vec3d(p.x + (sx * 0.58) + (fx * -0.3), 0.0D, p.z + (sz * 0.58) + (fz * -0.3));
                    Vec3d r = new Vec3d(p.x - (sx * 0.58) + (fx * -0.3), 0.0D, p.z - (sz * 0.58) + (fz * -0.3));
                    this.world.addParticle(ParticleTypes.SMOKE, l.x, p.y + 2.8, l.z, 0, 0, 0);
                    this.world.addParticle(ParticleTypes.SMOKE, r.x, p.y + 2.8, r.z, 0, 0, 0);
                }
            }
        }
    }

    @Nullable
    public Vec3d tickBeamTerrain(Vec3d origVec, Vec3d destVec) {
        RaycastContext rcc = new RaycastContext(origVec, destVec, ShapeType.COLLIDER, FluidHandling.NONE, this);
        BlockHitResult bhr = this.world.raycast(rcc);
        BlockPos bp = bhr.getBlockPos();
        BlockState bs = this.world.getBlockState(bp);
        //Server
        if (!this.world.isClient) {
            if (!this.getPassengerList().isEmpty()) {
                ServerPlayerEntity pilot = (ServerPlayerEntity) this.getPassengerList().get(0);
                if (!FlanCompat.hasPermission(world, bp, pilot)) {
                    return null;
                }
                if (!GOMLCompat.hasPermission(world, bp, pilot)) {
                    return null;
                }
            } else {
                return null;
            }
            if (bs.getBlock() == Blocks.AIR) {
                int bX = bp.getX();
                int bY = bp.getY();
                int bZ = bp.getZ();
                BlockPos belowBlockPos = new BlockPos(bX, bY - 1, bZ);
                BlockState belowBlockState = this.world.getBlockState(belowBlockPos);
                Material belowMaterial = belowBlockState.getMaterial();
                // For firebeam:
                // Set fire to block above if block is solid and above block is air.
                // Set block to fire if below block is solid and current block is air.
                // For icebeam:
                // Set ice on block hit if block is water.
                // Set ice to block below if hit block is air and below block is water.
                if (this.beamType == 1) {
                    if (world.getGameRules().getBoolean(MagitekMechs.BEAM_FIRE)) {
                        // Check if below block is solid, set current air to fire
                        if (belowMaterial.isSolid() && !belowMaterial.isLiquid()) {
                            this.world.setBlockState(bp, Blocks.FIRE.getDefaultState());
                        }
                    }
                } else if (this.beamType == 2) {
                    if (belowBlockState.getBlock() == Blocks.WATER) {
                        if (world.getGameRules().getBoolean(MagitekMechs.BEAM_ICE)) {
                            if (belowBlockState.getFluidState().isStill()) {
                                this.world.setBlockState(belowBlockPos, Blocks.ICE.getDefaultState());
                            }
                        }
                    } else if (belowBlockState.getBlock() == Blocks.LAVA) {
                        if (belowBlockState.getFluidState().isStill()) {
                            this.world.setBlockState(belowBlockPos, Blocks.OBSIDIAN.getDefaultState());
                        }
                    }
                }
            } else {
                if (this.beamType == 1) {
                    if (bs.getMaterial() == Material.WOOD && bs.getBlock().toString().contains("log")) {
                        this.smeltBlockPos = bp;
                        if (smeltProgress < 10) {
                            smeltProgress += 1;
                            // Increasing block break progress
                            this.world.setBlockBreakingInfo(this.getId(), smeltBlockPos, smeltProgress);
                        } else {
                            smeltProgress = 0;
                            // Server only
                            ItemScatterer.spawn(world, bp.getX(), bp.getY(), bp.getZ(), Items.CHARCOAL.getDefaultStack());
                            //this.world.breakBlock(smeltBlockPos, true);
                            this.world.setBlockState(bp, Blocks.AIR.getDefaultState());
                        }
                    }
                } else if (this.beamType == 2) {
                    if (bs.getBlock() == Blocks.WATER) {
                        if (world.getGameRules().getBoolean(MagitekMechs.BEAM_ICE)) {
                            if (bs.getFluidState().isStill()) {
                                this.world.setBlockState(bp, Blocks.ICE.getDefaultState());
                            }
                        }
                    } else if (bs.getBlock() == Blocks.LAVA) {

                        if (bs.getFluidState().isStill()) {
                            this.world.setBlockState(bp, Blocks.OBSIDIAN.getDefaultState());
                        }
                    } else if (bs.getBlock() == Blocks.FIRE) {
                        this.world.setBlockState(bp, Blocks.AIR.getDefaultState());
                    }
                }
            }
        }
        //Client
        else {
            if (this.smeltBlockPos == null) {
                this.smeltBlockPos = bp;
            } else {
                if (this.smeltBlockPos != bp) {
                    this.world.setBlockBreakingInfo(this.getId(), smeltBlockPos, 0);
                    this.smeltProgress = 0;
                    this.smeltBlockPos = bp;
                }
            }
            if (this.beamType == 1) {
                if (bs.getMaterial() == Material.WOOD && bs.getBlock().toString().contains("log")) {
                    if (smeltProgress < 10) {
                        smeltProgress += 1;
                        // Increasing block break progress
                        this.world.setBlockBreakingInfo(this.getId(), smeltBlockPos, smeltProgress);
                    } else {
                        smeltProgress = 0;
                    }
                }
            }
        }
        return bhr.getPos();
    }

    public void tickBeamEntities(Vec3d origVec, Vec3d destVec) {
        if (!this.world.isClient) {
            if (world.getGameRules().getBoolean(MagitekMechs.BEAM_DAMAGE)) {
                if (!this.getPassengerList().isEmpty()) {
                    ServerPlayerEntity pilot = (ServerPlayerEntity) this.getPassengerList().get(0);
                    Predicate<Entity> isPilot = Predicate.isEqual(pilot);
                    EntityHitResult ehr = null;
//                    if (pilot.handSwinging){
//                    }
//                    else {
//                    }
                    // Not sure why the number needs to be so large for the bounding box
                    Box newBox = this.getBoundingBox().stretch(this.getRotationVec(1.0F).multiply(40)).expand(1.0D, 1.0D, 1.0D);
                    ehr = ServerEntityHitResult.raycast(this, origVec, destVec, newBox, isPilot.negate(), 40);
                    if (ehr != null) {
                        BlockPos entityBlockPos = new BlockPos(ehr.getPos());
                        if (!FlanCompat.hasPermission(world, entityBlockPos, pilot)) {
                            return;
                        }
                        if (!GOMLCompat.hasPermission(world, entityBlockPos, pilot)) {
                            return;
                        }
                        if (this.beamType == 1) {
                            ehr.getEntity().damage(DamageSource.MAGIC, 2);
                        } else if (this.beamType == 2) {
                            ehr.getEntity().damage(DamageSource.MAGIC, 2);
                        }
                    }
                }
            }
        }
    }

    public void tickBeam() {
        this.currentTime = this.world.getTime();
        this.deltaTime = this.currentTime - this.lastTime;
        this.lastTime = this.currentTime;
        this.beamGraceTime += this.deltaTime;
        if (this.beamFiring == true) {
            if (this.beamGraceTime >= 2) {
                this.beamGraceTime = 0;
                // Beam start and end points.
                double yawRadians = Math.toRadians(this.getHeadYaw() + 90);
                double pitchRadians = Math.toRadians(-(Math.max(Math.min(this.getPitch(0), 20.0F), -20.0F)));
                double gX = Math.cos(pitchRadians) * Math.cos(yawRadians);
                double gY = Math.sin(pitchRadians);
                double gZ = Math.cos(pitchRadians) * Math.sin(yawRadians);
                Vec3d origVec = new Vec3d(0, 1.50, 0);
                origVec = origVec.add(this.getPos().x, this.getPos().y, this.getPos().z);
                Vec3d destVec = new Vec3d(0, 0, 0);
                destVec = destVec.add(gX, gY, gZ);
                destVec = destVec.multiply(6);
                destVec = destVec.add(origVec);

                Vec3d terrainDestVec = tickBeamTerrain(origVec, destVec);
                if (terrainDestVec == null) {
                    tickBeamEntities(origVec, destVec);
                } else {
                    tickBeamEntities(origVec, terrainDestVec);
                }

                if (this.deltaTime > 0) {
                    this.deltaGathered += this.deltaTime;
                }
                if (this.deltaGathered > 20) {
                    this.deltaGathered = 0;
                    this.beamFiring = false;
                    if (!this.world.isClient) {
                        this.dataSetBeamAllowed(true);
                    }
                    if (this.world.isClient) {
                        this.stopBeamSound();
                    }
                    // Clear block smelting progress
                    if (this.smeltBlockPos != null) {
                        this.world.setBlockBreakingInfo(this.getId(), smeltBlockPos, 0);
                        this.smeltProgress = 0;
                    }
                }
            }
        }
    }

    public void tickGravity() {
        if (!this.hasPassengers()) {
            if (this.canMoveVoluntarily() || this.isLogicalSideForUpdatingMovement()) {
                double d = -0.08 * 0.9800000190734863D;
                if (this.world.getBlockState(this.getBlockPos().add(0, -1, 0)).getBlock() == Blocks.AIR) {
                    this.setVelocity(this.getVelocity().multiply(0, 1, 0));
                    this.setVelocity(this.getVelocity().add(0, d, 0));
                    this.move(MovementType.SELF, this.getVelocity());
                } else {
                    this.setVelocity(new Vec3d(0, -0.3, 0));
                    this.move(MovementType.SELF, this.getVelocity());
                }
            }
        }
    }

    @Override
    public void tick() {
        this.tickGravity();
        this.tickBeam();
        this.tickFuel();

        int magitekSlotPD = this.propertyDelegate.get(0);
        boolean magitekSlotDT = this.dataGetIsOn();
        this.isOn = magitekSlotDT;

        if (this.world.isClient) {
            this.tickSmoke();
            this.energyStored = this.dataGetFuel();
        } else {
            if (magitekSlotPD == 1) {
                this.dataSetIsOn(true);
            } else {
                this.dataSetIsOn(false);
            }
        }
        if (!this.world.isClient) {
            if (this.isOn == true) {
                if (energyStored > 0) {

                } else {
                    this.isOn = false;
                }
            }
        }
        super.tick();
    }

    public void getDistanceForSound(){

    }

    @Environment(EnvType.CLIENT)
    public void makeFirstStepSound(double xoff, double zoff, double distance) {
        this.firststepsound = new MAFirstStepSoundInstance(this, MagitekMechs.mechstep, SoundCategory.AMBIENT, (float) (100.0F/distance),
                0.92F, this.getX() + xoff, this.getY(), this.getZ() + zoff);
        this.firststepsound.soundManager.play(this.firststepsound);
    }

    @Environment(EnvType.CLIENT)
    public void makeStepSound(double xoff, double zoff, double distance) {
        this.stepsound = new MAStepSoundInstance(this, MagitekMechs.mechsteptwo, SoundCategory.AMBIENT, (float) (100.0F/distance),
                0.92F, this.getX() + xoff, this.getY(), this.getZ() + zoff);
        this.stepsound.soundManager.play(this.stepsound);
    }

    @Environment(EnvType.CLIENT)
    public void makeBeamSound(double xoff, double zoff, double distance) {
        if (this.beamsound == null) {
            this.beamsound = new MABeamSoundInstance(this, MagitekMechs.firebeam, SoundCategory.AMBIENT, (float) (100.0F/distance),
                    0.92F, this.getX() + xoff, this.getY(), this.getZ() + zoff);
            this.beamsound.soundManager.play(this.beamsound);
        } else {
            if (this.beamFiring) {
                this.beamsound.moveSound();
            } else {
                this.stopBeamSound();
            }
        }
    }

    @Environment(EnvType.CLIENT)
    public void stopBeamSound() {
        if (this.beamsound != null) {
            this.beamsound.soundManager.stop(this.beamsound);
            this.beamsound = null;
        }
    }

    @Override
    public Vec3d updatePassengerForDismount(LivingEntity passenger) {
        if (!passenger.isDead()) {
        } else {
            // Set allowflying to true to stop "no-flying" after death.
            // As far as I can tell it resets to false after respawn.
            PlayerEntity player = (PlayerEntity) passenger;
            // ?: does this work without a setter.
            player.getAbilities().allowFlying = true;

        }
        return new Vec3d(this.getX(), this.getBoundingBox().maxY - 1.0, this.getZ());        // -1.0 for less head bonk.
    }

    @Override
    public void travel(Vec3d pos) {

        if (this.isAlive() && this.hasPassengers() && this.isOn && this.energyStored > 0) {
            LivingEntity livingentity = (LivingEntity) this.getControllingPassenger();
            this.setYaw(livingentity.getYaw());
            this.prevYaw = this.getYaw();
            this.setPitch(livingentity.getPitch() * 0.5F);
            this.setRotation(this.getYaw(), this.getPitch());
            this.setBodyYaw(this.getYaw());
            this.setHeadYaw(this.getYaw());
            float f = livingentity.sidewaysSpeed * 0.25F;
            float f1 = livingentity.forwardSpeed * 0.5F;
            if (f1 <= 0.0F) {
                f1 *= 0.25F;
            }
            this.setMovementSpeed(0.3F);
            super.travel(new Vec3d((double) f, pos.y, (double) f1));
        }
    }

    @Environment(EnvType.CLIENT)
    public void sendBeam() {
        if (this.dataGetBeamAllowed() && this.dataGetFuel() > 0 && this.dataGetIsOn()) {
            PacketByteBuf buf = PacketByteBufs.create();
            ClientPlayNetworking.send(NetIdentifiers.ma_p_to_server, buf);
        }
    }

    // sendBeam -> receiveBeamServer
    public static void receiveBeamServer(MinecraftServer server, ServerPlayerEntity
            player, ServerPlayNetworkHandler handler, PacketByteBuf buf, PacketSender responseSender) {
        Object vehicle = player.getVehicle();
        MagitekArmorEntity maentity = ((MagitekArmorEntity) vehicle);

        if (maentity.dataGetBeamAllowed()) {
            if (maentity.dataGetFuel() > 0 && maentity.dataGetIsOn() && maentity.syncedAddonSlotted > 0) {
                maentity.beamType = maentity.syncedAddonSlotted;
                maentity.dataSetBeamAllowed(false);
                maentity.fireBeam();

                // Send beam to other clients.
                Collection<ServerPlayerEntity> players = PlayerLookup.around(server.getWorld(player.world.getRegistryKey()), player.getPos(), 1000);
                for (ServerPlayerEntity otherplayer : players) {

                    PacketByteBuf bufid = PacketByteBufs.create();
                    bufid.writeInt(player.getId()); // Buffer now has a boolean and an entity id.
                    bufid.writeInt(maentity.syncedAddonSlotted); // Add what is essentially the beam type.

                    ServerPlayNetworking.send(otherplayer, NetIdentifiers.ma_p_to_clients, bufid);

                    //if (otherplayer != player) {
                    //	PacketByteBuf bufid = PacketByteBufs.create();
                    //	bufid.writeInt(player.getEntityId());
                    //	ServerPlayNetworking.send(otherplayer, NetIdentifiers.ma_p_to_clients, bufid);
                    //}
                }
            }
        }
    }

    //Clients receiving server relayed sendBeam.
    @Environment(EnvType.CLIENT)
    public static void receiveBeamClient(ClientPlayNetworkHandler handler1, PacketSender sender1, MinecraftClient
            client1, PacketByteBuf buf) {
        int entityid = buf.readInt();
        int beamtype = buf.readInt();
        if (client1.world != null) {
            Entity player = client1.world.getEntityById(entityid);
            if (player != null) {
                Object vehicle = player.getVehicle();
                if (vehicle != null) {
                    MagitekArmorEntity maentity = ((MagitekArmorEntity) vehicle);
                    maentity.beamType = beamtype;
                    maentity.fireBeam();

                }
            }
        }
    }

    @Environment(EnvType.CLIENT)
    public void clientBeamSound() {
        ClientPlayerEntity player = MinecraftClient.getInstance().player;
        if (this.hasPassenger(player)) {
            player.playSound(MagitekMechs.firebeam, 1, 1);
        } else {
            OtherClientPlayerEntity otherplayer = (OtherClientPlayerEntity) this.getFirstPassenger();
            double distancePre = Math.sqrt(Math.pow(player.getX() - otherplayer.getX(), 2) + Math.pow(player.getZ() - otherplayer.getZ(), 2));
            distancePre *= 1;
            double distance = 1.0F + distancePre;
            this.makeBeamSound(this.getX(), this.getY(), distance);
        }
    }

    public void fireBeam() {
        this.beamFiring = true;
        if (this.world.isClient) {
            this.stopBeamSound();
            this.resetBeamRender = true;
            this.clientBeamSound();
        }
    }

    @Environment(EnvType.CLIENT)
    public void clientOpeningSound(PlayerEntity player) {
        player.playSound(MagitekMechs.opening, 1, 1);
    }

    @Override
    public boolean handleAttack(Entity attacker) {
        if (!this.world.isClient()) {
            if (MagitekMechs.attachedMods.contains("techreborn")) {
                PlayerEntity player = (PlayerEntity) attacker;
                if (!world.getGameRules().getBoolean(MagitekMechs.MECH_OWNERSHIP) ||
                        player.getUuid().equals(this.dataGetOwner().orElse(new UUID(0L, 0L)))){
                    int slot = player.getInventory().selectedSlot;
                    ItemStack mainStack = player.getInventory().getStack(slot);
                    if (mainStack != null) {
                        if (Registry.ITEM.getId(mainStack.getItem()).toString().equals("techreborn:wrench")) {
                            this.dropInventory();
                            ItemScatterer.spawn(world, this.getX(), this.getY(), this.getZ(), MagitekMechs.MA_WHOLE_ITEM.getDefaultStack());
                            this.setRemoved(RemovalReason.DISCARDED);
                            return true;
                        }
                    }
                }
            }
        }
        return super.handleAttack(attacker);
    }

    @Override
    public ActionResult interactMob(PlayerEntity player, Hand hand) {
        if (this.dataGetOwner().isEmpty()) {
            this.writeOwner(player.getUuid());
        }
        if (world.getGameRules().getBoolean(MagitekMechs.MECH_OWNERSHIP)) {
            if (!player.getUuid().equals(this.dataGetOwner().orElse(null))) {
                player.sendMessage(new TranslatableText("misc.mtmechs.locked"), true);
                //System.out.println("client" + player.getUuid() + " " + this.dataGetOwner().orElse(null));
                return ActionResult.success(this.world.isClient);
            }
        }
        if (player.shouldCancelInteraction()) {
            ItemStack handstack = player.getStackInHand(hand);
            if (this.world.isClient) {
                clientOpeningSound(player);
            }
            this.openScreen(player, this.inventory, handstack);
            return ActionResult.success(this.world.isClient);
        } else {
            if (!this.world.isClient) {
                player.setYaw(this.getYaw());
                player.setPitch(this.getPitch());
                player.startRiding(this);
            }
            return ActionResult.success(this.world.isClient);
        }
    }

    private PropertyDelegate propertyDelegate = new PropertyDelegate() {
        @Override
        public int get(int index) {
            if (index == 0) {
                return syncedMagiciteSlotted;
            } else if (index == 1) {
                return syncedAddonSlotted;
            } else if (index == 2) {
                return syncedEnergyStored;
            }
            //else if (index == 3) {
            //	return syncedPowerDrawExists;
            //}
            //else if (index == 4) {
            //	return syncedPowerDrawFrame;
            //}
            else if (index == 5) {
                return syncedFuelSlotted;
            } else {
                return -1;
            }
        }

        @Override
        public void set(int index, int value) {
            if (index == 0) {
                syncedMagiciteSlotted = value;
            }
            if (index == 1) {
                syncedAddonSlotted = value;
            }
            if (index == 2) {
                syncedEnergyStored = value;
            }
            //if (index == 3) {
            //	syncedPowerDrawExists = value;
            //}
            //if (index == 4) {
            //	syncedPowerDrawFrame = value;
            //}
            if (index == 5) {
                syncedFuelSlotted = value;
            }
        }

        @Override
        public int size() {
            return 5;
        }
    };

    public ScreenHandler getScreenHandler(int syncId, PlayerInventory playerInventory) {
        return MagitekMechs.MA_SCREEN_HANDLER.create(syncId, playerInventory);
    }

    private void openScreen(PlayerEntity player, SimpleInventory minventory, ItemStack handstack) {
        if (player.world != null && !player.world.isClient) {
            player.openHandledScreen(new ExtendedScreenHandlerFactory() {
                @Override
                public void writeScreenOpeningData(ServerPlayerEntity serverPlayerEntity, PacketByteBuf packetByteBuf) {
                    packetByteBuf.writeItemStack(handstack);
                }

                @Override
                public Text getDisplayName() {
                    return new TranslatableText("inventory.mtmechs.magitekarmor");
                }

                @Override
                public @Nullable ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {

                    return new MagitekArmorScreenHandler(syncId, inv, minventory, propertyDelegate);
                }
            });
        }
    }

    public int getMagiciteSlot() {
        return 10;
    }

    public int getAddonSlot() {
        return 11;
    }

    public void writeOwner(UUID owner) {
        this.dataSetOwner(owner);
        NbtCompound compoundTag = new NbtCompound();
        compoundTag.putUuid("owner", owner);
        this.writeNbt(compoundTag);
    }

    @Override
    public void writeCustomDataToNbt(NbtCompound tag) {
        super.writeCustomDataToNbt(tag);

        // Save inventory state.
        NbtList listTag = new NbtList();
        for (int i = 0; i < this.inventory.size(); ++i) {
            ItemStack itemStack = this.inventory.getStack(i);
            if (!itemStack.isEmpty()) {
                NbtCompound compoundTag = new NbtCompound();
                compoundTag.putByte("Slot", (byte) i);
                itemStack.writeNbt(compoundTag);
                listTag.add(compoundTag);
            }
        }
        tag.put("Items", listTag);

        // Save energy state.
        NbtCompound energyStoredTag = new NbtCompound();
        energyStoredTag.putInt("EnergyStored", this.energyStored);
        NbtCompound energyMaxTag = new NbtCompound();
        energyMaxTag.putInt("EnergyMaximum", this.energyMaximum);
        NbtList energyListTag = new NbtList();
        energyListTag.add(energyStoredTag);
        energyListTag.add(energyMaxTag);
        tag.put("Energy", energyListTag);

        if (!this.dataGetOwner().isEmpty()) {
            tag.putUuid("owner", this.dataGetOwner().orElse(new UUID(0L, 0L)));
        }
    }

    @Override
    public void readCustomDataFromNbt(NbtCompound tag) {
        super.readCustomDataFromNbt(tag);
        NbtList listTag = tag.getList("Items", 10);
        for (int i = 0; i < listTag.size(); ++i) {
            NbtCompound compoundTag = listTag.getCompound(i);
            int j = compoundTag.getByte("Slot") & 255;
            if (j >= 0 && j < this.inventory.size()) {
                ItemStack is = ItemStack.fromNbt(compoundTag);

                if (j == this.getMagiciteSlot() && is.getItem() == MagitekMechs.MAGICITE_ITEM) {
                    this.dataSetIsOn(true);
                    this.propertyDelegate.set(0, 1);
                } else if (j == this.getAddonSlot()) {
                    if (is.getItem() == Items.BLAZE_ROD) {
                        this.propertyDelegate.set(1, 1);
                    } else if (is.getItem() == Items.SNOWBALL) {
                        this.propertyDelegate.set(1, 2);
                    }
                }

                this.inventory.setStack(j, is);
            }
        }
        NbtList energyListTag = tag.getList("Energy", NbtType.COMPOUND);
        NbtCompound energyStoredTag = energyListTag.getCompound(0);
        NbtCompound energyMaxTag = energyListTag.getCompound(1);
        this.energyStored = energyStoredTag.getInt("EnergyStored");
        this.energyMaximum = energyMaxTag.getInt("EnergyMaximum");
        if (tag.contains("owner")) {
            this.dataSetOwner(tag.getUuid("owner"));
            //System.out.println (String.valueOf(this.world.isClient() + " " + this.dataGetOwner()));
        }
    }

    @Override
    public boolean isFireImmune() {
        return true;
    }

    @Override
    public double getMountedHeightOffset() {
        return 1.25D;
    }

    @Override
    protected void dropInventory() {
        super.dropInventory();
        ItemScatterer.spawn(this.world, this.getBlockPos(), this.inventory);
    }

    @Override
    public boolean cannotDespawn() {
        return true;
    }

    @Override
    public boolean canBeControlledByRider() {
        return true;
    }

    @Override
    public boolean canBreatheInWater() {
        return true;
    }

    @Override
    public boolean canBeRiddenInWater() {
        return true;
    }

    @Override
    public void tickRiding() {
        super.tickRiding();
        // This is used for players and passengers, not vehicles.
    }

    @Override
    protected void playStepSound(BlockPos pos, BlockState state) {
    }
}