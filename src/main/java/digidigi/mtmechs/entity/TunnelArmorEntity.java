package digidigi.mtmechs.entity;

import digidigi.mtmechs.compat.FlanCompat;
import digidigi.mtmechs.compat.GOMLCompat;
import digidigi.mtmechs.sound.DrillingSoundInstance;
import digidigi.mtmechs.sound.GrindingSoundInstance;
import digidigi.mtmechs.sound.WheelSoundInstance;

import java.util.*;

import digidigi.mtmechs.MagitekMechs;
import digidigi.mtmechs.NetIdentifiers;
import digidigi.mtmechs.screen.TunnelArmorScreenHandler;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.networking.v1.PacketByteBufs;
import net.fabricmc.fabric.api.networking.v1.PacketSender;
import net.fabricmc.fabric.api.networking.v1.PlayerLookup;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.fabric.api.registry.FuelRegistry;
import net.fabricmc.fabric.api.screenhandler.v1.ExtendedScreenHandlerFactory;
import net.fabricmc.fabric.api.util.NbtType;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.client.MinecraftClient;
import net.minecraft.client.network.ClientPlayNetworkHandler;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.MovementType;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.mob.PathAwareEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.nbt.NbtList;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayNetworkHandler;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.sound.SoundCategory;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;
import software.bernie.geckolib3.core.AnimationState;
import software.bernie.geckolib3.core.IAnimatable;
import software.bernie.geckolib3.core.PlayState;
import software.bernie.geckolib3.core.builder.AnimationBuilder;
import software.bernie.geckolib3.core.controller.AnimationController;
import software.bernie.geckolib3.core.event.predicate.AnimationEvent;
import software.bernie.geckolib3.core.manager.AnimationData;
import software.bernie.geckolib3.core.manager.AnimationFactory;
import software.bernie.geckolib3.resource.GeckoLibCache;

import org.jetbrains.annotations.Nullable;

public class TunnelArmorEntity extends PathAwareEntity implements IAnimatable {

	private AnimationFactory factory = new AnimationFactory(this);
	
	public int syncedMagiciteSlotted = -1;		// Magicite exists in Magicite slot
	public int syncedFuelSlotted = 0;			// 1 if fuel existing in slot(s)
	public int syncedEnergyStored = 0;			// 0-41 Amount of energy remaining in reserve tank (or ongoing burn).
	public int syncedAddonSlotted = 0;     	// Valid Addon exists in Addon slot.

	public int energyStored = 0; 				// Actual energy value stored.
	public int energyMaximum = 0; 				// Burned fuel sets its own cap.
	
	public static TrackedData<Byte> TA_FLAGS;
	public static TrackedData<Integer> ENERGY_STORED;
	public static TrackedData<Boolean> DRILL_GRINDING;
	public static TrackedData<Boolean> WHEELS_ROLLING;
	public static TrackedData<Optional<UUID>> OWNER;

	public double lastcollectspeed = 0;
	public double collectspeed = 0; // Referenced in Model
	public double moving = 0; // Referenced in Model
	public WheelSoundInstance rollsound;
	public GrindingSoundInstance grindsound;
	public DrillingSoundInstance drillsound;

	public boolean isGrinding = false;
	public boolean isRolling = false;
	public boolean isDrilling = false;
	public boolean isLifting = false;
	public boolean isLifted = false;
	public int liftTimer = 0; // out of .5 seconds of the full lift animation
	
	public boolean animLift = false;
	
	public boolean isOn = false;
	
	public List<BlockPos> blocksPositions = new ArrayList<BlockPos>();
	public List<Integer> blocksProgress = new ArrayList<Integer>();
	public List<BlockPos> newBlocksPositions = new ArrayList<BlockPos>();
	public List<Integer> newBlocksProgress = new ArrayList<Integer>();

	public SimpleInventory inventory;

	public int smokecounter = 0;
	
	private long serverCurrentTime;
	private long serverDeltaTime;
	private long serverLastTime;

	static {
		TA_FLAGS = DataTracker.registerData(TunnelArmorEntity.class, TrackedDataHandlerRegistry.BYTE);
		ENERGY_STORED = DataTracker.registerData(TunnelArmorEntity.class, TrackedDataHandlerRegistry.INTEGER);
		DRILL_GRINDING = DataTracker.registerData(TunnelArmorEntity.class, TrackedDataHandlerRegistry.BOOLEAN);
		WHEELS_ROLLING = DataTracker.registerData(TunnelArmorEntity.class, TrackedDataHandlerRegistry.BOOLEAN);
		OWNER = DataTracker.registerData(TunnelArmorEntity.class, TrackedDataHandlerRegistry.OPTIONAL_UUID);
	}

	protected void initDataTracker() {
		super.initDataTracker();
		this.dataTracker.startTracking(TA_FLAGS, (byte) 0);
		this.dataTracker.startTracking(ENERGY_STORED, 0);
		this.dataTracker.startTracking(DRILL_GRINDING, (boolean) false);
		this.dataTracker.startTracking(WHEELS_ROLLING, (boolean) false);
		this.dataTracker.startTracking(OWNER, Optional.empty());
	}

	public void dataSetIsOn(boolean truth) {
		if (this.isOn != truth) {
			byte b = (Byte) this.dataTracker.get(TA_FLAGS);
			this.dataTracker.set(TA_FLAGS, truth ? (byte) (b | 1) : (byte) (b & -2));
			this.isOn = truth;
		}
	}
	public boolean dataGetIsOn() {
		return ((Byte) this.dataTracker.get(TA_FLAGS) & 1) != 0;
	}
	public void dataSetDrilling(boolean truth) {
		if (this.isDrilling != truth) {
			byte b = (Byte) this.dataTracker.get(TA_FLAGS);
			this.dataTracker.set(TA_FLAGS, truth ? (byte) (b | 2) : (byte) (b & -3));
			this.isDrilling = truth;
		}
	}
	public boolean dataGetDrilling() {
		return ((Byte)this.dataTracker.get(TA_FLAGS) & 2) != 0;
	}
	public void dataSetLifting(boolean truth) {
		if (this.isLifting != truth) {
			byte b = (Byte) this.dataTracker.get(TA_FLAGS);
			this.dataTracker.set(TA_FLAGS, truth ? (byte) (b | 4) : (byte) (b & -5));
			this.isLifting = truth;
		}
	}
	public boolean dataGetLifting() {
		return ((Byte)this.dataTracker.get(TA_FLAGS) & 4) != 0;
	}
	public void dataSetLifted(boolean truth) {
		if (this.isLifted != truth) {
			byte b = (Byte) this.dataTracker.get(TA_FLAGS);
			this.dataTracker.set(TA_FLAGS, truth ? (byte) (b | 8) : (byte) (b & -9));
			this.isLifted = truth;
		}
	}
	public boolean dataGetLifted() {
		return ((Byte)this.dataTracker.get(TA_FLAGS) & 8) != 0;
	}
	public void dataSetFuel(int energy) {
		this.dataTracker.set(ENERGY_STORED, energy);
	}
	public int dataGetFuel() {
		return this.dataTracker.get(ENERGY_STORED);
	}
	public void dataSetDrillGrinding(boolean truth) {
		this.dataTracker.set(DRILL_GRINDING, truth);
	}
	public boolean dataGetDrillGrinding() {
		return this.dataTracker.get(DRILL_GRINDING);
	}
	public void dataSetOwner(UUID owner) { this.dataTracker.set(OWNER, Optional.of(owner));};
	public Optional<UUID> dataGetOwner() { return this.dataTracker.get(OWNER);}

	public boolean hasSidewaysVelocity() {
		if (this.getVelocity().x != 0.0 | this.getVelocity().z != 0.0) {
			return true;
		} else {
			return false;
		}
	}

	private <E extends IAnimatable> PlayState predicateLift(AnimationEvent<E> event) {
		
		if (!this.dataGetIsOn() || this.dataGetFuel() == 0 || !this.dataGetDrilling()) {
			event.getController().setAnimation(new AnimationBuilder().addAnimation("animation.tunnelarmor.liftdefault", true));
			this.animLift = false;
			return PlayState.CONTINUE;
		}
		
		if (this.dataGetLifted()) {
			event.getController().setAnimation(new AnimationBuilder().addAnimation("animation.tunnelarmor.liftextended", true));
			return PlayState.CONTINUE;
		}
		
		if (event.getController().getAnimationState() == AnimationState.Stopped) {
			event.getController().setAnimation(new AnimationBuilder().addAnimation("animation.tunnelarmor.liftextended", true));
			return PlayState.CONTINUE;
		}
		
		if (this.dataGetLifting()) {
			if (this.animLift == false) {
				event.getController().setAnimation(new AnimationBuilder().addAnimation("animation.tunnelarmor.lift", false));
				this.animLift = true;
				return PlayState.CONTINUE;
			}
		}
		
		return PlayState.CONTINUE;
		
		
	}

	private <E extends IAnimatable> PlayState predicateDrill(AnimationEvent<E> event) {
		
		if (!this.dataGetIsOn() || this.dataGetFuel() == 0) {
			event.getController().setAnimation(new AnimationBuilder().addAnimation("animation.tunnelarmor.liftdefault", true));
			return PlayState.STOP;
		}
		
		if (this.dataGetDrilling() == true) {
			event.getController()
					.setAnimation(new AnimationBuilder().addAnimation("animation.tunnelarmor.drills", true));
			return PlayState.CONTINUE;
		} else {
			return PlayState.STOP;
		}
	}

	public double getforward() {
		double vx = this.getVelocity().getX();
		double vy = 0.0D;
		double vz = this.getVelocity().getZ();
		Vec3d velocity = new Vec3d(vx, vy, vz);

		// i'm getting forward velocity even when moving perpendicular
		// maybe get the vector from the yaw directly?
		// ..doesn't seem to be helping.

		vx = this.getHorizontalFacing().getVector().getX();
		vz = this.getHorizontalFacing().getVector().getZ();
		Vec3d facing = new Vec3d(vx, vy, vz);
		double dot = velocity.dotProduct(facing);
		return dot;
	}

	private <E extends IAnimatable> PlayState predicate(AnimationEvent<E> event) {
		if (this.hasSidewaysVelocity() && !this.getPassengerList().isEmpty() && this.energyStored > 0) {
			this.moving = 1;
			this.lastcollectspeed = this.collectspeed;
			this.collectspeed += this.getforward() * 25;
		} else {
			this.moving = 0;
		}
		GeckoLibCache.getInstance().parser.setValue("variable.collectspeed", this.collectspeed);
		GeckoLibCache.getInstance().parser.setValue("variable.moving", this.moving);

		event.getController()
				.setAnimation(new AnimationBuilder().addAnimation("animation.tunnelarmor.wheelsforward", true));
		return PlayState.CONTINUE;
	}

	@Override
	public void registerControllers(AnimationData data) {
		data.addAnimationController(new AnimationController(this, "controller", 0, this::predicate));
		data.addAnimationController(new AnimationController(this, "controllerDrill", 0, this::predicateDrill));
		data.addAnimationController(new AnimationController(this, "controllerLift", 0, this::predicateLift));
	}

	@Override
	public AnimationFactory getFactory() {
		return this.factory;
	}

	public TunnelArmorEntity(EntityType<? extends PathAwareEntity> entityType, World world) {
		super(entityType, world);
		this.stepHeight = 1.0F;
		this.setYaw(0F);
		this.setSneaking(true);
		this.inventory = new SimpleInventory(12);
	}

	@Nullable
	public Entity getControllingPassenger() {
		return this.getPassengerList().isEmpty() ? null : this.getPassengerList().get(0);
	}

	@Environment(EnvType.CLIENT)
	public void tickSmoke() {
		if (this.world.isClient && this.energyStored > 0) {
			this.smokecounter += 1;
			if (this.smokecounter == 5) {
				this.smokecounter = 0;
			}
			// if (this.world.isClient && this.smokecounter % 5 == 0) {
			if (this.smokecounter == 0) {
				Vec3d p = this.getPos();
				double aim = Math.toRadians(this.getYaw() * -1);
				// forward vector to add
				double fx = Math.sin(aim);
				double fz = Math.cos(aim);
				// leftward side vector to add or subtract
				double sx = fz;
				double sz = fx * -1;

				if (this.isRolling) {
					Vec3d lp = new Vec3d(p.x + (sx * 0.32) + (fx * -1.1), 0.0D, p.z + (sz * 0.32) + (fz * -1.1));
					Vec3d rp = new Vec3d(p.x - (sx * 0.32) + (fx * -1.1), 0.0D, p.z - (sz * 0.32) + (fz * -1.1));
					this.world.addParticle(ParticleTypes.SMOKE, lp.x, p.y + 2.0, lp.z, 0 - (fx * 0.05), 0,
							0 - (fz * 0.05));
					this.world.addParticle(ParticleTypes.SMOKE, rp.x, p.y + 2.0, rp.z, 0 - (fx * 0.05), 0,
							0 - (fz * 0.05));
				}
				if (this.isOn) {
					Vec3d blp = new Vec3d(p.x + (sx * 0.77) + (fx * -0.77), 0.0D, p.z + (sz * 0.77) + (fz * -0.77));
					Vec3d brp = new Vec3d(p.x - (sx * 0.77) + (fx * -0.77), 0.0D, p.z - (sz * 0.77) + (fz * -0.77));
					this.world.addParticle(ParticleTypes.SMOKE, blp.x, p.y + 2.2, blp.z, 0 - (fx * 0.05), 0,
							0 - (fz * 0.05));
					this.world.addParticle(ParticleTypes.SMOKE, brp.x, p.y + 2.2, brp.z, 0 - (fx * 0.05), 0,
							0 - (fz * 0.05));
				}
				if (this.isDrilling) {
					Vec3d flp = new Vec3d(p.x + (sx * 0.77) + (fx * -0.17), 0.0D, p.z + (sz * 0.77) + (fz * -0.17));
					Vec3d frp = new Vec3d(p.x - (sx * 0.77) + (fx * -0.17), 0.0D, p.z - (sz * 0.77) + (fz * -0.17));
					this.world.addParticle(ParticleTypes.SMOKE, flp.x, p.y + 2.3, flp.z, 0, 0, 0);
					this.world.addParticle(ParticleTypes.SMOKE, frp.x, p.y + 2.3, frp.z, 0, 0, 0);
				}
			}
		}
	}

	public boolean checkIfAir(BlockPos bp) {
		BlockState bs = this.world.getBlockState(bp);
		
		float hardness = bs.getHardness(world, bp);
		if (!bs.isAir()) {
			if (hardness > 0.0) {
				return false;
			}
			else {
				return true;
			}
		}
		return true;
		 
	}

	public int updateBlockProgress(BlockPos currentblock, int existingprog) {
		if (existingprog < 10) {
			existingprog += 1;
			// Increasing block break progress
			this.world.setBlockBreakingInfo(this.getId(), currentblock, existingprog);
		} else {
			existingprog = 0;
			if (!this.world.isClient) {
				this.world.breakBlock(currentblock, true);
			}
		}
		return existingprog;
	}

	public void manageBlocks(BlockPos bp) {
		
		BlockState bs = this.world.getBlockState(bp);
		float hardness = bs.getHardness(world, bp);
		
		if (hardness > 0.0) {
			if (this.blocksPositions.contains(bp)) {
				int idx = this.blocksPositions.indexOf(bp);
				newBlocksPositions.add(bp);
				int prog = updateBlockProgress(bp, this.blocksProgress.get(idx));
				newBlocksProgress.add(prog);
			} else {
				newBlocksPositions.add(bp);
				int prog = updateBlockProgress(bp, 0);
				newBlocksProgress.add(prog);
			}
		}
	}
	
	public void tickFuel() {
		if (!this.world.isClient) {
			int spEnergyStored = 0;
			if (energyStored == 0) {
				if (this.syncedMagiciteSlotted == 1) {
					ItemStack fuelStackA = this.inventory.getStack(9);
					ItemStack fuelStackB = this.inventory.getStack(10);
					if (fuelStackA != null && fuelStackB != null) {
						Item fuelItemA = fuelStackA.getItem();
						Item fuelItemB = fuelStackB.getItem();
						if (fuelItemA != null && fuelItemA == fuelItemB) {
							if (FuelRegistry.INSTANCE.get(fuelItemA) != null) {
								// Here the item is fuel and it has a value.
								int fuelAmount = FuelRegistry.INSTANCE.get(fuelItemA);
								// Remove and expend the fuel.
								this.inventory.removeStack(9, 1);
								this.inventory.removeStack(10, 1);
								energyStored += fuelAmount;
								energyMaximum = fuelAmount;
							}
						}
					}
				}
			}
			else {
				energyStored -= 1;
				if (energyStored < 0) {
					energyStored = 0;
				}
				float dv = (float) (energyMaximum / 10.0);
				spEnergyStored = (int) Math.ceil(energyStored / dv);
			}
			this.propertyDelegate.set(2, spEnergyStored);
			this.dataSetFuel(this.energyStored);
		}
	}

	public void tickDrill() {
		if (this.getPassengerList().isEmpty()) {
			this.isRolling = false;
		} else {
			if (this.isDrilling && this.isOn) {
				// No inconvenient sloping while drilling is active.
				this.stepHeight = 0F;
				double aim = Math.toRadians(this.getYaw() * -1);
				double fx = Math.sin(aim);
				double fz = Math.cos(aim);
				Vec3d tapos = this.getPos();
				Vec3d robpos = new Vec3d(tapos.x + (fx * 2), tapos.y, tapos.z + (fz * 2));
				Vec3d roblpos = new Vec3d(robpos.x + (fz), robpos.y, robpos.z + (fx * -1));
				Vec3d robrpos = new Vec3d(robpos.x + (fz * -1), robpos.y, robpos.z + (fx));
				Vec3d rompos = new Vec3d(tapos.x + (fx * 2), robpos.y + 1, tapos.z + (fz * 2));
				Vec3d rolpos = new Vec3d(robpos.x + (fz), robpos.y + 1, robpos.z + (fx * -1));
				Vec3d rorpos = new Vec3d(robpos.x + (fz * -1), robpos.y + 1, robpos.z + (fx));
				Vec3d rotpos = new Vec3d(tapos.x + (fx * 2), robpos.y + 2, tapos.z + (fz * 2));
				Vec3d rotlpos = new Vec3d(robpos.x + (fz), robpos.y + 2, robpos.z + (fx * -1));
				Vec3d rotrpos = new Vec3d(robpos.x + (fz * -1), robpos.y + 2, robpos.z + (fx));
				BlockPos bp_rob = new BlockPos(robpos);
				BlockPos bp_robl = new BlockPos(roblpos);
				BlockPos bp_robr = new BlockPos(robrpos);
				BlockPos bp_rom = new BlockPos(rompos);
				BlockPos bp_rol = new BlockPos(rolpos);
				BlockPos bp_ror = new BlockPos(rorpos);
				BlockPos bp_rot = new BlockPos(rotpos);
				BlockPos bp_rotl = new BlockPos(rotlpos);
				BlockPos bp_rotr = new BlockPos(rotrpos);
				if (!this.getPassengerList().isEmpty()) {
					if (!this.world.isClient) {
						ServerPlayerEntity pilot = (ServerPlayerEntity) this.getPassengerList().get(0);
						if (!FlanCompat.hasPermission(world, bp_rob, pilot)) {
							return;
						}
						if (!GOMLCompat.hasPermission(world, bp_rob, pilot)) {
							return;
						}
						if (!FlanCompat.hasPermission(world, bp_robl, pilot)) {
							return;
						}
						if (!GOMLCompat.hasPermission(world, bp_robl, pilot)) {
							return;
						}
						if (!FlanCompat.hasPermission(world, bp_robr, pilot)) {
							return;
						}
						if (!GOMLCompat.hasPermission(world, bp_robr, pilot)) {
							return;
						}
						if (!FlanCompat.hasPermission(world, bp_rom, pilot)) {
							return;
						}
						if (!GOMLCompat.hasPermission(world, bp_rom, pilot)) {
							return;
						}
						if (!FlanCompat.hasPermission(world, bp_rol, pilot)) {
							return;
						}
						if (!GOMLCompat.hasPermission(world, bp_rol, pilot)) {
							return;
						}
						if (!FlanCompat.hasPermission(world, bp_ror, pilot)) {
							return;
						}
						if (!GOMLCompat.hasPermission(world, bp_ror, pilot)) {
							return;
						}
						if (!FlanCompat.hasPermission(world, bp_rot, pilot)) {
							return;
						}
						if (!GOMLCompat.hasPermission(world, bp_rot, pilot)) {
							return;
						}
						if (!FlanCompat.hasPermission(world, bp_rotl, pilot)) {
							return;
						}
						if (!GOMLCompat.hasPermission(world, bp_rotl, pilot)) {
							return;
						}
						if (!FlanCompat.hasPermission(world, bp_rotr, pilot)) {
							return;
						}
						if (!GOMLCompat.hasPermission(world, bp_rotr, pilot)) {
							return;
						}
					}
				}
				else {
					return;
				}
				if (this.world.isClient) {
					this.makeDrillSound(fx, fz);
					if (!this.isGrinding) {
						boolean blockIsAir = true;
						blockIsAir = this.checkIfAir(bp_rob) & blockIsAir;
						blockIsAir = this.checkIfAir(bp_robl) & blockIsAir;
						blockIsAir = this.checkIfAir(bp_robr) & blockIsAir;
						blockIsAir = this.checkIfAir(bp_rom) & blockIsAir;
						blockIsAir = this.checkIfAir(bp_rol) & blockIsAir;
						blockIsAir = this.checkIfAir(bp_ror) & blockIsAir;
						blockIsAir = this.checkIfAir(bp_rot) & blockIsAir;
						blockIsAir = this.checkIfAir(bp_rotl) & blockIsAir;
						blockIsAir = this.checkIfAir(bp_rotr) & blockIsAir;
						if (blockIsAir == false) {
							this.isGrinding = true;
							this.makeGrindSound(fx, fz);
						}
					} else {
						boolean blockIsAir = true;
						blockIsAir = this.checkIfAir(bp_rob) & blockIsAir;
						blockIsAir = this.checkIfAir(bp_robl) & blockIsAir;
						blockIsAir = this.checkIfAir(bp_robr) & blockIsAir;
						blockIsAir = this.checkIfAir(bp_rom) & blockIsAir;
						blockIsAir = this.checkIfAir(bp_rol) & blockIsAir;
						blockIsAir = this.checkIfAir(bp_ror) & blockIsAir;
						blockIsAir = this.checkIfAir(bp_rot) & blockIsAir;
						blockIsAir = this.checkIfAir(bp_rotl) & blockIsAir;
						blockIsAir = this.checkIfAir(bp_rotr) & blockIsAir;
						if (blockIsAir == true) {
							this.isGrinding = false;
							this.stopGrindSound();
						}
						else {
						}
					}
				}
				// Reset temporary list.
				if (!newBlocksPositions.isEmpty()) {
					newBlocksPositions.clear();
					newBlocksProgress.clear();
				}
				this.manageBlocks(bp_rob);
				this.manageBlocks(bp_robl);
				this.manageBlocks(bp_robr);
				this.manageBlocks(bp_rom);
				this.manageBlocks(bp_rol);
				this.manageBlocks(bp_ror);
				this.manageBlocks(bp_rot);
				this.manageBlocks(bp_rotl);
				this.manageBlocks(bp_rotr);
				if (!this.blocksPositions.isEmpty()) {
					this.blocksPositions.clear();
					this.blocksProgress.clear();
				}
				this.blocksPositions.addAll(newBlocksPositions);
				this.blocksProgress.addAll(newBlocksProgress);

				if (!this.blocksPositions.isEmpty()) {
					this.dataSetDrillGrinding(true);
				}
				else {
					this.dataSetDrillGrinding(false);
				}
			} else {
				this.stepHeight = 1F;
			}
		}
	}

	public void tickGravity() {
		if (!this.hasPassengers()) {
			if (this.canMoveVoluntarily() || this.isLogicalSideForUpdatingMovement()) {
				double d = -0.08 * 0.9800000190734863D;
				if (this.world.getBlockState(this.getBlockPos().add(0, -1, 0)).getBlock() == Blocks.AIR) {
					this.setVelocity(this.getVelocity().multiply(0, 1, 0));
					this.setVelocity(this.getVelocity().add(0, d, 0));
					this.move(MovementType.SELF, this.getVelocity());
				} else {
					this.setVelocity(new Vec3d(0, -0.3, 0));
					this.move(MovementType.SELF, this.getVelocity());
				}
			}
		}
	}

	@Override
	public void tick() {
		this.tickGravity();
		this.tickFuel();
		this.isOn = this.dataGetIsOn();

		int magitekSlotPD = this.propertyDelegate.get(0);
		boolean magitekSlotDT = this.dataGetIsOn();
		this.isOn = magitekSlotDT;

		if (this.world.isClient) {
			this.tickSmoke();
			this.energyStored = this.dataGetFuel();
		}
		else {
			if (magitekSlotPD == 1) {
				this.dataSetIsOn(true);
			}
			else {
				this.dataSetIsOn(false);
			}
		}
		if (!this.world.isClient) {
			this.serverCurrentTime = System.currentTimeMillis();
			this.serverDeltaTime = this.serverCurrentTime - this.serverLastTime;
			this.serverLastTime = this.serverCurrentTime;

			if (this.dataGetLifting() && !this.dataGetLifted()) {
				if (this.liftTimer < 500)
				{
					this.liftTimer += this.serverDeltaTime;
				}
				else {
					this.dataSetLifting(false);
					this.dataSetLifted(true);
					this.liftTimer = 0;
				}
			}

			if (this.isOn == true) {
				if (energyStored > 0) {

				}
				else {
					this.isOn = false;
				}
			}
		}
		// Client
		else {
			this.continueDrillSound();
			this.continueGrindSound();
		}
		super.tick();
		this.tickDrill();

		if (this.world.isClient) {
			if ((int) this.collectspeed != (int) this.lastcollectspeed) {

				if (!this.isRolling) {
					if (!this.getPassengerList().isEmpty()) {
						this.isRolling = true;
					}
				}
			} else {
				if (this.isRolling) {
					this.stopRollSound();
					this.isRolling = false;
				}
			}
		}
	}
	
	@Environment(EnvType.CLIENT)
	public void makeRollSound(double xoff, double zoff) {
//		if (this.rollsound == null) {
//			this.rollsound = new WheelSoundInstance(this, MagitekMechs.roll_loop, SoundCategory.AMBIENT, 1.0F,
//					1.0F, this.getX() + xoff, this.getY(), this.getZ() + zoff);
//			this.rollsound.soundManager.play(this.rollsound);
//		}
//		else {
//			this.rollsound.moveSound();
//		}
	}
	@Environment(EnvType.CLIENT)
	public void stopRollSound() {
		if (this.rollsound != null) {
			this.rollsound.soundManager.stop(this.rollsound);
			this.rollsound = null;
		}
	}
	@Environment(EnvType.CLIENT)
	public void continueRollSound() {
//		if (this.isOn) {
//			if (this.dataGetRolling()) {
//				if (this.rollsound != null) {
//					this.rollsound.tick();
//				}
//			}
//			else {
//				this.stopRollSound();
//			}
//		}
	}
	@Environment(EnvType.CLIENT)
	public void makeGrindSound(double xoff, double zoff) {
		if (this.grindsound == null) {
			this.grindsound = new GrindingSoundInstance(this, MagitekMechs.grind_loop, SoundCategory.AMBIENT, 100.0F,
					1.0F, this.getX() + xoff, this.getY(), this.getZ() + zoff);
			this.grindsound.soundManager.play(this.grindsound);
		}
		else {
			this.grindsound.moveSound();
		}
	}
	@Environment(EnvType.CLIENT)
	public void stopGrindSound() {
		if (this.grindsound != null) {
			this.grindsound.soundManager.stop(this.grindsound);
			this.grindsound = null;
		}
	}
	@Environment(EnvType.CLIENT)
	public void continueGrindSound() {
		if (this.isOn) {
			if (this.dataGetDrillGrinding()) {
				if (this.grindsound != null) {
					this.grindsound.tick();
				}
			}
			else {
				this.stopGrindSound();
			}
		}
	}
	@Environment(EnvType.CLIENT)
	public void makeDrillSound(double xoff, double zoff) {
		if (this.drillsound == null) {
			this.drillsound = new DrillingSoundInstance(this, MagitekMechs.drill_loop, SoundCategory.AMBIENT, 100.0F,
					0.92F, this.getX() + xoff, this.getY(), this.getZ() + zoff);
			this.drillsound.soundManager.play(this.drillsound);
		}
		else {
			this.drillsound.finished = false;
			this.drillsound.moveSound();
		}

	}
	@Environment(EnvType.CLIENT)
	public void stopDrillSound() {
		if (this.drillsound != null) {
			this.drillsound.soundManager.stop(this.drillsound);
			this.drillsound = null;
		}
	}
	@Environment(EnvType.CLIENT)
	public void continueDrillSound() {
		this.isDrilling = this.dataGetDrilling();
		if (this.isOn) {
			if (this.isDrilling) {
				if (this.drillsound != null) {
					this.drillsound.tick();
				}
			}
			else {
				this.stopDrillSound();
			}
		}
	}
	
	@Environment(EnvType.CLIENT)
	public void stopSounds() {
		if (this.drillsound != null) {
			this.drillsound.soundManager.stop(this.drillsound);
			this.drillsound.finished = true;
		}
		if (this.grindsound != null) {
			this.grindsound.soundManager.stop(this.grindsound);
			this.grindsound.finished = true;
		}
		if (this.rollsound != null) {
			this.rollsound.soundManager.stop(this.rollsound);
			this.rollsound.finished = true;
		}
	}
	
	// button press ->
	@Environment(EnvType.CLIENT)
	public void drillToggle() {
		if (this.isDrilling == false) {
			if (!this.dataGetIsOn() || this.dataGetFuel() == 0) {
			}
			else {
				this.isDrilling = true;
				this.isLifting = true;
				this.sendDrillToggle(true);
			}
		} else {
			this.isDrilling = false;
			this.isLifting = false;
			this.sendDrillToggle(false);
		}
	}
	// button press -> drillToggle ->
	@Environment(EnvType.CLIENT)
	public void sendDrillToggle(Boolean toggle) {
		PacketByteBuf buf = PacketByteBufs.create();
		buf.writeBoolean(toggle);
		ClientPlayNetworking.send(NetIdentifiers.ta_p_to_server, buf);
		if (toggle == false) {
			this.stopGrindSound();
			this.stopDrillSound();
		}
	}
	// button press -> drillToggle -> sendDrillToggle ->
	public static void receiveDrillToggleServer(MinecraftServer server, ServerPlayerEntity player,
			ServerPlayNetworkHandler handler, PacketByteBuf buf, PacketSender responseSender) {
		Boolean drillstate = buf.readBoolean();
		Object vehicle = player.getVehicle();
		TunnelArmorEntity taentity = ((TunnelArmorEntity) vehicle);
		if (drillstate == false) {
			taentity.liftTimer = 0;
			taentity.dataSetLifting(false);
			taentity.dataSetLifted(false);
			taentity.dataSetDrilling(false);
			taentity.dataSetDrillGrinding(false);
		}
		else {
			taentity.dataSetLifting(true);
			taentity.dataSetLifted(false);
			taentity.dataSetDrilling(true); // Drilling is activated before the animation has fully extended, but that's fine.
		}
	}
	
	@Environment(EnvType.CLIENT)
	public static void receiveDismount(ClientPlayNetworkHandler handler1, PacketSender sender1, MinecraftClient client1,
			PacketByteBuf buf) {
		int entityid = buf.readInt();
		if (client1.world != null) {
			Entity player = client1.world.getEntityById(entityid);
			if (player != null) {
				Object vehicle = player.getVehicle();
				if (vehicle != null) {
					player.getVehicle().getId();
					TunnelArmorEntity taentity = ((TunnelArmorEntity) vehicle);
					if (taentity != null) {
						taentity.isGrinding = false;
						taentity.isRolling = false;
						taentity.isDrilling = false;
						taentity.isLifting = false;
						taentity.stopGrindSound();
						taentity.stopDrillSound();
					}
				}
			}
		}
	}

	// Server method called by a server player entity -> vehicle.
	@Override
	public Vec3d updatePassengerForDismount(LivingEntity passenger) {
		this.isGrinding = false;
		this.isRolling = false;
		
		this.dataSetDrilling(false);
		this.dataSetLifting(false);
		this.dataSetLifted(false);
		
		MinecraftServer server = this.world.getServer();
		ServerPlayerEntity player = ((ServerPlayerEntity) passenger);
		Collection<ServerPlayerEntity> players = PlayerLookup.around(server.getWorld(player.world.getRegistryKey()),
				player.getPos(), 1000);
		for (ServerPlayerEntity otherplayer : players) {
			if (otherplayer != player)
			{
				PacketByteBuf buf2 = PacketByteBufs.create();
				buf2.writeInt(player.getId());
				ServerPlayNetworking.send(otherplayer, NetIdentifiers.ta_dismount_to_clients, buf2);
			}
		}
		return new Vec3d(this.getX(), this.getBoundingBox().maxY - 1.0, this.getZ());
	}

	@Override
	public void travel(Vec3d pos) {
		if (this.isAlive() && this.hasPassengers() && this.isOn) {
			LivingEntity livingentity = (LivingEntity) this.getControllingPassenger();
			this.setYaw(livingentity.getYaw());
			this.prevYaw = this.getYaw();
			this.setPitch(livingentity.getPitch() * 0.5F);
			this.setRotation(this.getYaw(), this.getPitch());
			this.setBodyYaw(this.getYaw());
			this.setHeadYaw(this.getYaw());
			float f = livingentity.sidewaysSpeed * 0.25F;
			float f1 = livingentity.forwardSpeed * 0.5F;
			if (f1 <= 0.0F) {
				f1 *= 0.25F;
			}
			this.setMovementSpeed(0.3F);
			super.travel(new Vec3d((double) f, pos.y, (double) f1));
		}
	}

	@Environment(EnvType.CLIENT)
	public void clientOpeningSound(PlayerEntity player) {
		player.playSound(MagitekMechs.opening, 1, 1);
	}

	@Override
	public boolean handleAttack(Entity attacker) {
		if (!this.world.isClient()){
			if (MagitekMechs.attachedMods.contains("techreborn")) {
				PlayerEntity player = (PlayerEntity) attacker;
				if (!world.getGameRules().getBoolean(MagitekMechs.MECH_OWNERSHIP) ||
						player.getUuid().equals(this.dataGetOwner().orElse(new UUID(0L, 0L)))){
					int slot = player.getInventory().selectedSlot;
					ItemStack mainStack = player.getInventory().getStack(slot);
					if (mainStack != null) {
						if (Registry.ITEM.getId(mainStack.getItem()).toString().equals("techreborn:wrench")){
							this.dropInventory();
							ItemScatterer.spawn(world, this.getX(), this.getY(), this.getZ(), MagitekMechs.TA_WHOLE_ITEM.getDefaultStack());
							this.setRemoved(RemovalReason.DISCARDED);
							return true;
						}
					}
				}
			}
		}
		return super.handleAttack(attacker);
	}

	@Nullable
	@Override
	public Entity getPrimaryPassenger() {
		return super.getPrimaryPassenger();
		// ?: Supposedly smoother movement, but moving up a block is stuttery.
//		if (this.hasPassengers()) {
//			return this.getPassengerList().get(0);
//		}
//		return null;
	}

	@Override
	public ActionResult interactMob(PlayerEntity player, Hand hand) {
		if (this.dataGetOwner().isEmpty()){
			this.writeOwner(player.getUuid());
		}
		if (world.getGameRules().getBoolean(MagitekMechs.MECH_OWNERSHIP)) {
			if (!player.getUuid().equals(this.dataGetOwner().orElse(null))) {
				player.sendMessage(new TranslatableText("misc.mtmechs.locked"), true);
				//System.out.println("client" + player.getUuid() + " " + this.dataGetOwner().orElse(null));
				return ActionResult.success(this.world.isClient);
			}
		}
		if (player.shouldCancelInteraction()) {
			ItemStack handstack = player.getStackInHand(hand);
			if (this.world.isClient) {
				clientOpeningSound(player);
			}
			this.openScreen(player, this.inventory, handstack);
			return ActionResult.success(this.world.isClient);
		}
		else {
			if (!this.world.isClient) {
				player.setYaw(this.getYaw());
				player.setPitch(this.getPitch());
				player.startRiding(this);
			}
			return ActionResult.success(this.world.isClient);
		}
	}
	
    private PropertyDelegate propertyDelegate = new PropertyDelegate() {
        @Override
        public int get(int index) {
        	if (index == 0) {
        		return syncedMagiciteSlotted;
        	}
        	else if (index == 1) {
        		return syncedAddonSlotted;
        	}
        	else if (index == 2) {
        		return syncedEnergyStored;
        	}
        	//else if (index == 5) {
        	//	return syncedFueledSlots;
        	//}
        	else {
        		return -1;
        	}
        }
 
        @Override
        public void set(int index, int value) {
        	if (index == 0) {
        		syncedMagiciteSlotted = value;
        	}
        	if (index == 1) {
        		syncedAddonSlotted = value;
        	}
        	if (index == 2) {
        		syncedEnergyStored = value;
        	}
        }
 
        @Override
        public int size() {
            return 5;
        }
    };

	public ScreenHandler getScreenHandler(int syncId, PlayerInventory playerInventory) {
		return MagitekMechs.TA_SCREEN_HANDLER.create(syncId, playerInventory);
	}
	

	private void openScreen(PlayerEntity player, SimpleInventory minventory, ItemStack handstack) {
		if (player.world != null && !player.world.isClient) {
			
			player.openHandledScreen(new ExtendedScreenHandlerFactory() {
				@Override
				public void writeScreenOpeningData(ServerPlayerEntity serverPlayerEntity, PacketByteBuf packetByteBuf) {
					packetByteBuf.writeItemStack(handstack);
				}
				@Override
				public Text getDisplayName() {
					return new TranslatableText("inventory.mtmechs.tunnelarmor");
				}
				@Override
				public @Nullable ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {

					return new TunnelArmorScreenHandler(syncId, inv, minventory, propertyDelegate);
				}
			});
		}
	}
	

	public int getMagiciteSlot() {
		return 11;
	}

	public void writeOwner(UUID owner) {
		this.dataSetOwner(owner);
		NbtCompound compoundTag = new NbtCompound();
		compoundTag.putUuid("owner", owner);
		this.writeNbt(compoundTag);
	}

	@Override
	public void writeCustomDataToNbt(NbtCompound tag) {
		super.writeCustomDataToNbt(tag);
		
		// Save inventory state.
        NbtList listTag = new NbtList();
        for(int i = 0; i < this.inventory.size(); ++i) {
            ItemStack itemStack = this.inventory.getStack(i);
            if (!itemStack.isEmpty()) {
               NbtCompound compoundTag = new NbtCompound();
               compoundTag.putByte("Slot", (byte)i);
               itemStack.writeNbt(compoundTag);
               listTag.add(compoundTag);
            }
        }
        tag.put("Items", listTag);
        
        // Save energy state.
        NbtCompound energyStoredTag = new NbtCompound();
        energyStoredTag.putInt("EnergyStored", this.energyStored);
        NbtCompound energyMaxTag = new NbtCompound();
        energyMaxTag.putInt("EnergyMaximum", this.energyMaximum);
        NbtList energyListTag = new NbtList();
        energyListTag.add(energyStoredTag);
        energyListTag.add(energyMaxTag);
        tag.put("Energy", energyListTag);

		if (!this.dataGetOwner().isEmpty()){
			tag.putUuid("owner", this.dataGetOwner().orElse(new UUID(0L, 0L)));
		}
	}
	
	@Override
	public void readCustomDataFromNbt(NbtCompound tag) {
		super.readCustomDataFromNbt(tag);
		NbtList listTag = tag.getList("Items", 10);
		for(int i = 0; i < listTag.size(); ++i) {
			NbtCompound compoundTag = listTag.getCompound(i);
			int j = compoundTag.getByte("Slot") & 255;
			if (j >= 0 && j < this.inventory.size()) {
				ItemStack is = ItemStack.fromNbt(compoundTag);
				if (j == this.getMagiciteSlot() && is.getItem() == MagitekMechs.MAGICITE_ITEM) {
					this.dataSetIsOn(true);
					this.propertyDelegate.set(0, 1);
				}
				this.inventory.setStack(j, is);
			}
		}
		NbtList energyListTag = tag.getList("Energy", NbtType.COMPOUND);
        NbtCompound energyStoredTag = energyListTag.getCompound(0);
        NbtCompound energyMaxTag = energyListTag.getCompound(1);
        this.energyStored = energyStoredTag.getInt("EnergyStored");
        this.energyMaximum = energyMaxTag.getInt("EnergyMaximum");
		if (tag.contains("owner")) {
			this.dataSetOwner(tag.getUuid("owner"));
			//System.out.println (String.valueOf(this.world.isClient() + " " + this.dataGetOwner()));
		}

	}

	@Override
	public boolean isInvulnerableTo(DamageSource damageSource) {
		if (damageSource == DamageSource.IN_WALL){
			return true;
		}
		else{
			return super.isInvulnerableTo(damageSource);
		}
	}

	@Override
	public void onDeath(DamageSource source) {
		super.onDeath(source);
		if (this.world.isClient){
			this.stopSounds();
		}
	}
	@Override
	public boolean isFireImmune() {
		return true;
	}
	@Override
	public double getMountedHeightOffset() {
		return 0.75D;
	}
	@Override
	protected void dropInventory() {
		super.dropInventory();
		ItemScatterer.spawn(this.world, this.getBlockPos(), this.inventory);
	}
	@Override
	public boolean cannotDespawn() {
		return true;
	}
	@Override
	public boolean canBeControlledByRider() {
		return true;
	}
	@Override
	public boolean canBreatheInWater() {
		return true;
	}
	@Override
	public boolean canBeRiddenInWater() {
		return true;
	}
	@Override
	protected void playStepSound(BlockPos pos, BlockState state) {
	}
}