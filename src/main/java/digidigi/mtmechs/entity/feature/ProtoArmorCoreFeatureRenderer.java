package digidigi.mtmechs.entity.feature;

import digidigi.mtmechs.entity.ProtoArmorEntity;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.OverlayTexture;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.VertexConsumerProvider;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.Entity;
import net.minecraft.util.Identifier;
import software.bernie.geckolib3.core.IAnimatable;
import software.bernie.geckolib3.geo.render.built.GeoModel;
import software.bernie.geckolib3.model.AnimatedGeoModel;
import software.bernie.geckolib3.model.provider.GeoModelProvider;
import software.bernie.geckolib3.renderers.geo.GeoLayerRenderer;
import software.bernie.geckolib3.renderers.geo.IGeoRenderer;

@Environment(EnvType.CLIENT)
public class ProtoArmorCoreFeatureRenderer <T extends Entity &  IAnimatable> extends GeoLayerRenderer implements IGeoRenderer<T> {
	AnimatedGeoModel geoModelProvider;
	private static final Identifier brighttexture = new Identifier("mtmechs", "textures/entity/protoarmor_core.png");
	private static final RenderLayer brightlayer = RenderLayer.getEyes(brighttexture);
	
	
	public ProtoArmorCoreFeatureRenderer(IGeoRenderer<T> entityRendererIn, AnimatedGeoModel modelProvider) {
		super(entityRendererIn);
		this.geoModelProvider = modelProvider;
	}

	public GeoModelProvider getGeoModelProvider() {
		return geoModelProvider;
	}

	public Identifier getTextureLocation(T instance) {
		return brighttexture;
	}

	public void render(MatrixStack matrixStackIn, VertexConsumerProvider bufferIn, int packedLightIn,
			Entity entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks,
			float netHeadYaw, float headPitch) {
		
		ProtoArmorEntity paentity = ((ProtoArmorEntity) entitylivingbaseIn);
		if (paentity.isOn) {
			GeoModel model = geoModelProvider.getModel(geoModelProvider.getModelLocation(entitylivingbaseIn));
			render(model, (T) entitylivingbaseIn, partialTicks, brightlayer, matrixStackIn, bufferIn, null, 15728640, OverlayTexture.DEFAULT_UV, 1.0F, 1.0F, 1.0F, 1.0F);
		}
	}
	
}