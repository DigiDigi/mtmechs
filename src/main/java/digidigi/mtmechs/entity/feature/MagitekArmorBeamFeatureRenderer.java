package digidigi.mtmechs.entity.feature;

import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;

import digidigi.mtmechs.entity.MagitekArmorEntity;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.render.*;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.Entity;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.Matrix3f;
import net.minecraft.util.math.Matrix4f;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3f;
import software.bernie.geckolib3.core.IAnimatable;
import software.bernie.geckolib3.geo.render.built.GeoModel;
import software.bernie.geckolib3.model.AnimatedGeoModel;
import software.bernie.geckolib3.model.provider.GeoModelProvider;
import software.bernie.geckolib3.renderers.geo.GeoLayerRenderer;
import software.bernie.geckolib3.renderers.geo.IGeoRenderer;

@Environment(EnvType.CLIENT)
public class MagitekArmorBeamFeatureRenderer <T extends Entity &  IAnimatable> extends GeoLayerRenderer implements IGeoRenderer<T> {
	AnimatedGeoModel geoModelProvider;
	private static final Identifier FIREBEAM_TEXTURE = new Identifier("mtmechs", "textures/entity/firebeam_big.png");
	private static final Identifier ICEBEAM_TEXTURE = new Identifier("mtmechs", "textures/entity/icebeam_big.png");

	public long allDelta = 0;
	public long deltaGathered = 0;
	public long deltaTime = 0;
	public long lastTime = 0;
	public long currentTime = 0;

	public int scaleSteps = 0;
	public int scaleMaxSteps = 15;
	public int animationSteps = 0;
	public int animationMaxSteps = 20;
	public boolean scaleComplete = false;

	public int beamType = 0;

	public MagitekArmorBeamFeatureRenderer(IGeoRenderer<T> entityRendererIn, AnimatedGeoModel modelProvider) {
		super(entityRendererIn);
		this.geoModelProvider = modelProvider;
	}

	public GeoModelProvider getGeoModelProvider() {
		return geoModelProvider;
	}
	public Identifier getTextureLocation(T instance) {
		return FIREBEAM_TEXTURE;
	}

	public void render(GeoModel model, T animatable, float partialTicks, RenderLayer type, MatrixStack matrixStackIn,
			VertexConsumerProvider renderTypeBuffer, VertexConsumer vertexBuilder, int packedLightIn,
			int packedOverlayIn, float red, float green, float blue, float alpha) {
		IGeoRenderer.super.render(model, animatable, partialTicks, type, matrixStackIn, renderTypeBuffer, vertexBuilder,
				packedLightIn, packedOverlayIn, red, green, blue, alpha);
	}

	private void renderBeam(MagitekArmorEntity entity, float partialTicks, MatrixStack matrixStack,
			VertexConsumerProvider bufferIn, int packedLightIn) {

		float h = 20;
		float j = 1;
		float k = j * 0.5F % 1.0F;
		float l = 1.625F;
		matrixStack.push();

		//0.0625 cube pixel size
		//0.5 beam widths
		//0.25 half beam widths

		// Put beam on core
		matrixStack.translate(-0.250D, (double)l, -0.750D);

		if (entity.resetBeamRender) {
			this.deltaGathered = 0;
			this.animationSteps = 0;
			this.scaleComplete = false;
			entity.resetBeamRender = false;
		}

		this.currentTime = entity.world.getTime();
		this.deltaTime = this.currentTime - this.lastTime;
		this.lastTime = this.currentTime;
		if (this.deltaTime > 0) {
			this.deltaGathered += this.deltaTime;
		}

		if (this.deltaGathered > 0) {
			if (!this.scaleComplete) {
				this.scaleSteps += 1;
			}

			this.animationSteps += 1;
			if (this.scaleSteps >= this.scaleMaxSteps) {
				this.scaleComplete = true;

			}
			this.deltaGathered = 0;
		}

		float scalefactor = this.scaleSteps * 0.0625F;
		float pOffset = -11F;
		float pMulti = 2.3F;
		float adjustedPitch = (entity.getPitch(0) * pMulti)+pOffset;
		double pitchClamped = -(Math.max(Math.min(adjustedPitch, 20.0F), -20.0F));
		// Apply pitch to beam.
		matrixStack.multiply(Vec3f.POSITIVE_X.getDegreesQuaternion((int) pitchClamped));

		// Widen beam over time.
		matrixStack.translate(0.250F, -0.250F, 0);
		matrixStack.scale(0.0625F+scalefactor, 0.0625F+scalefactor, 1.F);
		matrixStack.translate(-0.250F, 0.250F, 0);

		// Rotate beam.
		matrixStack.translate(0.250F, -0.250F, 0);
		matrixStack.multiply(Vec3f.POSITIVE_Z.getDegreesQuaternion(this.animationSteps * 24));
		matrixStack.translate(-0.250F, 0.250F, 0);

		Vec3d vec3d2 = Vec3d.ZERO;
		Vec3d vec3d = new Vec3d(vec3d2.x, vec3d2.y, vec3d2.z);
		Vec3d vec3d3 = vec3d.subtract(vec3d2);
		float m = (float)(vec3d3.length() + 1.0D);
		vec3d3 = vec3d3.normalize();
		float n = (float)Math.acos(vec3d3.y);
		float o = (float)Math.atan2(vec3d3.z, vec3d3.x);
		matrixStack.multiply(Vec3f.POSITIVE_Y.getDegreesQuaternion((1.5707964F - o) * 57.295776F));
		matrixStack.multiply(Vec3f.POSITIVE_X.getDegreesQuaternion(n * 57.295776F));

		float r = h * h;
		int R = 64 + (int)(r * 191.0F);
		int G = 32 + (int)(r * 191.0F);
		int B = 128 - (int)(r * 64.0F);
		float aq = -1.0F + k;
		float ar = m * 2.5F + aq;
//		RenderPhase.Transparency NO_TRANSPARENCY = new RenderPhase.Transparency("no_transparency", () -> {
//			RenderSystem.disableBlend();
//		}, () -> {
//		});
//		RenderPhase.Transparency ADDITIVE_TRANSPARENCY = new RenderPhase.Transparency("additive_transparency", () -> {
//			RenderSystem.enableBlend();
//			RenderSystem.blendFunc(GlStateManager.SrcFactor.ONE, GlStateManager.DstFactor.ONE);
//		}, () -> {
//			RenderSystem.disableBlend();
//			RenderSystem.defaultBlendFunc();
//		});
//		RenderPhase.Transparency TRANSLUCENT_TRANSPARENCY = new RenderPhase.Transparency("translucent_transparency", () -> {
//			RenderSystem.enableBlend();
//			RenderSystem.blendFuncSeparate(GlStateManager.SrcFactor.SRC_ALPHA, GlStateManager.DstFactor.ONE_MINUS_SRC_ALPHA, GlStateManager.SrcFactor.ONE, GlStateManager.DstFactor.ONE_MINUS_SRC_ALPHA);
//		}, () -> {
//			RenderSystem.disableBlend();
//			RenderSystem.defaultBlendFunc();
//		});
//		RenderPhase.Transparency GLINT_TRANSPARENCY = new RenderPhase.Transparency("glint_transparency", () -> {
//			RenderSystem.enableBlend();
//			RenderSystem.blendFuncSeparate(GlStateManager.SrcFactor.SRC_COLOR, GlStateManager.DstFactor.ONE, GlStateManager.SrcFactor.ZERO, GlStateManager.DstFactor.ONE);
//		}, () -> {
//			RenderSystem.disableBlend();
//			RenderSystem.defaultBlendFunc();
//		});
//		RenderPhase.Transparency CRUMBLING_TRANSPARENCY = new RenderPhase.Transparency("crumbling_transparency", () -> {
//			RenderSystem.enableBlend();
//			RenderSystem.blendFuncSeparate(GlStateManager.SrcFactor.DST_COLOR, GlStateManager.DstFactor.SRC_COLOR, GlStateManager.SrcFactor.ONE, GlStateManager.DstFactor.ZERO);
//		}, () -> {
//			RenderSystem.disableBlend();
//			RenderSystem.defaultBlendFunc();
//		});
//		RenderPhase.Transparency LIGHTNING_TRANSPARENCY = new RenderPhase.Transparency("lightning_transparency", () -> {
//			RenderSystem.enableBlend();
//			RenderSystem.blendFunc(GlStateManager.SrcFactor.SRC_ALPHA, GlStateManager.DstFactor.ONE);
//		}, () -> {
//			RenderSystem.disableBlend();
//			RenderSystem.defaultBlendFunc();
//		});
		//RenderPhase.DiffuseLighting ENABLE_DIFFUSE_LIGHTING = new RenderPhase.DiffuseLighting(true);
		//RenderPhase.Alpha ONE_TENTH_ALPHA = new RenderPhase.Alpha(0.003921569F);
//		RenderPhase.Cull DISABLE_CULLING = new RenderPhase.Cull(false);
//		RenderPhase.Lightmap ENABLE_LIGHTMAP = new RenderPhase.Lightmap(true);
//		RenderPhase.Overlay ENABLE_OVERLAY_COLOR = new RenderPhase.Overlay(true);
//		boolean affectsOutline = true;
//
		Identifier beamTexture;
		if (this.beamType < 2) {
			beamTexture = FIREBEAM_TEXTURE;
		}
		else {
			beamTexture = ICEBEAM_TEXTURE;
		}

//		//Access Widener is used (/src/main/resource/mtmechs.accesswidener) to make the following accessible.
//		RenderLayer.MultiPhaseParameters rlmpp = RenderLayer.MultiPhaseParameters.builder().texture(
//				new RenderPhase.Texture(
//						beamTexture,
//						false,
//						false
//						)
//				).transparency(ADDITIVE_TRANSPARENCY)
//				.cull(DISABLE_CULLING)
//				.lightmap(ENABLE_LIGHTMAP)
//				.overlay(ENABLE_OVERLAY_COLOR)
//				.build(affectsOutline);

		//		.diffuseLighting(ENABLE_DIFFUSE_LIGHTING)
		// 		.alpha(ONE_TENTH_ALPHA)


//		RenderLayer customRenderLayer = RenderLayer.of("entity_translucent", VertexFormats.POSITION_COLOR_TEXTURE_OVERLAY_LIGHT_NORMAL, VertexFormat.DrawMode.QUADS, 256, false, true, rlmpp);
		RenderLayer customRenderLayer = RenderLayer.getEyes(beamTexture);
		VertexConsumer vertexConsumer = bufferIn.getBuffer(customRenderLayer);
//		VertexConsumer vertexConsumer = bufferIn.getBuffer(customRenderLayer);
		MatrixStack.Entry entry = matrixStack.peek();
		Matrix4f matrix4f = entry.getModel();
		Matrix3f matrix3f = entry.getNormal();

		// "left" face                                     X     Y     Z
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 0.0F, 0.0F, 0.0F, R, G, B, 0.5F, ar);
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 6.0F, 0.0F, 0.0F, R, G, B, 0.5F, aq);
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 6.0F, 0.5F, 0.0F, R, G, B, 0.0F, aq);
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 0.0F, 0.5F, 0.0F, R, G, B, 0.0F, ar);
		// "top" face
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 6.0F, 0.5F, 0.0F, R, G, B, 0.0F, aq);
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 0.0F, 0.5F, 0.0F, R, G, B, 0.0F, ar);
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 0.0F, 0.5F, 0.5F, R, G, B, 0.5F, ar);
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 6.0F, 0.5F, 0.5F, R, G, B, 0.5F, aq);
		// "front" face
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 0.0F, 0.0F, 0.5F, R, G, B, 0.0F, aq);
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 0.0F, 0.5F, 0.5F, R, G, B, 0.0F, ar);
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 0.0F, 0.5F, 0.0F, R, G, B, 0.5F, ar);
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 0.0F, 0.0F, 0.0F, R, G, B, 0.5F, aq);
		// "back" face
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 6.0F, 0.0F, 0.5F, R, G, B, 0.0F, aq);
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 6.0F, 0.5F, 0.5F, R, G, B, 0.0F, ar);
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 6.0F, 0.5F, 0.0F, R, G, B, 0.5F, ar);
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 6.0F, 0.0F, 0.0F, R, G, B, 0.5F, aq);
		// "bottom" face
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 0.0F, 0.0F, 0.5F, R, G, B, 0.0F, aq);
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 6.0F, 0.0F, 0.5F, R, G, B, 0.0F, ar);
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 6.0F, 0.0F, 0.0F, R, G, B, 0.5F, ar);
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 0.0F, 0.0F, 0.0F, R, G, B, 0.5F, aq);
		// "right" face
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 6.0F, 0.5F, 0.5F, R, G, B, 0.5F, ar);
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 0.0F, 0.5F, 0.5F, R, G, B, 0.5F, aq);
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 0.0F, 0.0F, 0.5F, R, G, B, 0.0F, aq);
		renderConsumer(vertexConsumer, matrix4f, matrix3f, 6.0F, 0.0F, 0.5F, R, G, B, 0.0F, ar);

		matrixStack.pop();
	}

	public void renderConsumer(VertexConsumer vertexConsumer, Matrix4f matrix4f, Matrix3f matrix3f, float f, float g, float h, int i, int j, int k, float l, float m) {
		vertexConsumer.vertex(matrix4f, f, g, h).color(i, j, k, 128).texture(l, m).overlay(OverlayTexture.DEFAULT_UV).light(15728880).normal(matrix3f, 0.0F, 1.0F, 0.0F).next();
	}

	@Override
	public void render(MatrixStack matrixStackIn, VertexConsumerProvider bufferIn, int packedLightIn,
			Entity entitylivingbaseIn, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks,
			float netHeadYaw, float headPitch) {

		MagitekArmorEntity maentity = (MagitekArmorEntity)entitylivingbaseIn;
		this.beamType = maentity.beamType;

		// Reset beam to minimum size.
		if (maentity.resetBeamRender == true) {
			this.scaleComplete = false;
			this.scaleSteps = 0;
			this.animationSteps = 0;
			maentity.resetBeamRender = false;
		}
		// Render only while beam is firing.
		if (maentity.beamFiring == true) {
			this.renderBeam(maentity, partialTicks, matrixStackIn, bufferIn, packedLightIn);
		}
	}
}