package digidigi.mtmechs;

import digidigi.mtmechs.client.renderer.TunnelArmorEntityRenderer;
import digidigi.mtmechs.entity.TunnelArmorEntity;
import digidigi.mtmechs.entity.MagitekArmorEntity;
import digidigi.mtmechs.entity.ProtoArmorEntity;
import digidigi.mtmechs.screen.MagitekArmorScreen;
import digidigi.mtmechs.screen.ProtoArmorScreen;
import digidigi.mtmechs.screen.TunnelArmorScreen;
import digidigi.mtmechs.client.renderer.MagitekArmorEntityRenderer;
import digidigi.mtmechs.client.renderer.ProtoArmorEntityRenderer;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.Environment;
import net.fabricmc.api.EnvType;
import net.fabricmc.fabric.api.client.event.lifecycle.v1.ClientTickEvents;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayConnectionEvents;
import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking;
import net.fabricmc.fabric.api.client.rendering.v1.EntityRendererRegistry;
import net.fabricmc.fabric.api.client.screenhandler.v1.ScreenRegistry;

@Environment(EnvType.CLIENT)
public class MagitekMechsClient implements ClientModInitializer {

    @Override
    public void onInitializeClient() {

		EntityRendererRegistry.register(MagitekMechs.MAGITEKARMOR, (context) -> {
			return new MagitekArmorEntityRenderer(context);
		});

        EntityRendererRegistry.register(MagitekMechs.PROTOARMOR, (context) -> {
            return new ProtoArmorEntityRenderer(context);
        });
        EntityRendererRegistry.register(MagitekMechs.TUNNELARMOR, (context) -> {
            return new TunnelArmorEntityRenderer(context);
        });
        
        ScreenRegistry.register(MagitekMechs.MA_SCREEN_HANDLER, MagitekArmorScreen::new);
        ScreenRegistry.register(MagitekMechs.PA_SCREEN_HANDLER, ProtoArmorScreen::new);
        ScreenRegistry.register(MagitekMechs.TA_SCREEN_HANDLER, TunnelArmorScreen::new);
		
		ClientPlayConnectionEvents.INIT.register((handler, client) -> {
			ClientPlayNetworking.registerReceiver(NetIdentifiers.ta_dismount_to_clients, (client1, handler1, buf, sender1) -> TunnelArmorEntity.receiveDismount(handler1, sender1, client1, buf));
		});
		
		ClientPlayConnectionEvents.INIT.register((handler, client) -> {
			ClientPlayNetworking.registerReceiver(NetIdentifiers.ma_p_to_clients, (client1, handler1, buf, sender1) -> MagitekArmorEntity.receiveBeamClient(handler1, sender1, client1, buf));
		});
		
		ClientPlayConnectionEvents.INIT.register((handler, client) -> {
			ClientPlayNetworking.registerReceiver(NetIdentifiers.pa_p_to_clients, (client1, handler1, buf, sender1) -> ProtoArmorEntity.receiveBeamClient(handler1, sender1, client1, buf));
		});
        
        KeyBindings.KeyBindsInit();
        
    	//	Pattern for firing only once on input.
    	//	KeyBindings.mtkPrimary.isPressed() true
        //		if keylatch false
    	//			do action in response
    	// 			enable key latch
    	//	else
    	//		disable key latch
        
//        ClientTickEvents.END_CLIENT_TICK.register(client -> {
//            if (KeyBindings.mtkDebug.wasPressed()) {
//            //client.player.sendChatMessage("/summon mtmechs:firebeam");
//        	//client.player.sendMessage(new LiteralText("-mtmechs debug keybind-"), false);
//            }
//        });
        ClientTickEvents.END_CLIENT_TICK.register(client -> {
            if (KeyBindings.mtkPrimary.isPressed()) {
            	if (KeyLatch.mtkPrimary == false) {
        			if (client.player.hasVehicle()) {
        				Object vehicle = client.player.getVehicle();
        				if (vehicle instanceof TunnelArmorEntity) {
        					TunnelArmorEntity taentity = ((TunnelArmorEntity) vehicle);
        					taentity.drillToggle();

        				}
        				else if (vehicle instanceof MagitekArmorEntity) {
        					MagitekArmorEntity maentity = ((MagitekArmorEntity) vehicle);
        					maentity.sendBeam();
        				}
        				else if (vehicle instanceof ProtoArmorEntity) {
        					ProtoArmorEntity paentity = ((ProtoArmorEntity) vehicle);
        					paentity.sendBeam();
        				}
        			}
        			KeyLatch.mtkPrimary = true;
            	}
            }
            else {
            	KeyLatch.mtkPrimary = false;
            }
            
        });
        
    }
}