package digidigi.mtmechs.screen;

import digidigi.mtmechs.MagitekMechs;
import net.fabricmc.fabric.api.transfer.v1.context.ContainerItemContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ArrayPropertyDelegate;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.util.math.BlockPos;
import team.reborn.energy.Energy;
import team.reborn.energy.api.EnergyStorage;
import team.reborn.energy.api.EnergyStorageUtil;
import team.reborn.energy.api.base.SimpleBatteryItem;

public class ProtoArmorScreenHandler extends ScreenHandler {
	private final Inventory inventory;
	PropertyDelegate propertyDelegate;
	
	private BlockPos pos;

	public Slot energySlot;
	
	public int magiciteSlotIndex = 0;
	public int addonSlotIndex = 1;
	public int energySlotIndex = -1;
	
	public ProtoArmorScreenHandler(int syncId, PlayerInventory playerInventory, PacketByteBuf buf) {
		this(syncId, playerInventory, new SimpleInventory(12), new ArrayPropertyDelegate(5));
	}

	public ProtoArmorScreenHandler(int syncId, PlayerInventory playerInventory, Inventory inventory, PropertyDelegate propertyDelegate) {
		super(MagitekMechs.PA_SCREEN_HANDLER, syncId);
		checkSize(inventory, 12);
		this.inventory = inventory;
		this.propertyDelegate = propertyDelegate;
		this.addProperties(propertyDelegate);
		
		inventory.onOpen(playerInventory.player);

		int m;
		int l;
		
		// Battery Slot
		if (!MagitekMechs.trInstalled) {
			magiciteSlotIndex = 0;
			addonSlotIndex = 1;
			
		}
		else {
			energySlotIndex = 0;
			magiciteSlotIndex = 1;
			addonSlotIndex = 2;
			this.energySlot = new Slot(inventory, 9, 35, 46);
			this.addSlot(this.energySlot);
		}

		// Core Slot
		this.addSlot(new Slot(inventory, 10, 125, 46));
		// Magic Addon
		this.addSlot(new Slot(inventory, 11, 147, 46));

		// Storage
		for (m = 0; m < 3; ++m) {
			for (l = 0; l < 3; ++l) {
				this.addSlot(new Slot(inventory, l + m * 3, 62 + l * 18, 18 + m * 18));
			}
		}
		//The player inventory
		for (m = 0; m < 3; ++m) {
			for (l = 0; l < 9; ++l) {
				this.addSlot(new Slot(playerInventory, l + m * 9 + 9, 8 + l * 18, 84 + m * 18));
			}
		}
		//The player Hotbar
		for (m = 0; m < 9; ++m) {
			this.addSlot(new Slot(playerInventory, m, 8 + m * 18, 142));
		}

	}
	
	public int getSyncedMagiciteSlotted() {
		return propertyDelegate.get(0);
	}
	
	public int getSyncedAddonSlotted() {
		return propertyDelegate.get(1);
	}
	
	public int getSyncedEnergyStored() {
		return propertyDelegate.get(2);
	}
	
	public int getSyncedEnergySlotted() {
		return propertyDelegate.get(3);
	}
	
	public int getSyncedPowerDrawFrame() {
		return propertyDelegate.get(4);
	}

	@Override
	public boolean canInsertIntoSlot(Slot slot) {
		return super.canInsertIntoSlot(slot);
	}
	public BlockPos getPos() {
		return pos;
	}
	@Override
	public boolean canUse(PlayerEntity player) {
		return this.inventory.canPlayerUse(player);
	}
	
	@Override
	public Slot getSlot(int index) {

		if (index == this.magiciteSlotIndex) {
			this.propertyDelegate.set(0, 0);
			Slot magiciteSlot = this.slots.get(this.magiciteSlotIndex);
			if (magiciteSlot != null && magiciteSlot.hasStack()) {
				ItemStack magiciteStack = magiciteSlot.getStack();
				if (magiciteStack.getItem() == MagitekMechs.MAGICITE_ITEM) {
					this.propertyDelegate.set(0, 1); // Magicite in slot.
					this.propertyDelegate.set(4, 0); // Reset power draw animation
				}
			}
		}
		if (index == this.addonSlotIndex) {
			Slot addonSlot = this.slots.get(this.addonSlotIndex);
			this.propertyDelegate.set(1, 0);
			if (addonSlot != null && addonSlot.hasStack()) {
				ItemStack addonStack = addonSlot.getStack();
				
				Item stackItem = addonStack.getItem();
				if (stackItem == Items.BLAZE_ROD) {
					this.propertyDelegate.set(1, 1);
				}
				else if (stackItem == Items.SNOWBALL) {
					this.propertyDelegate.set(1, 2);
				}
			}
		}

		if (index == this.energySlotIndex) {
			Slot energySlot = this.slots.get(this.energySlotIndex);
			if (energySlot != null && energySlot.hasStack()) {

				ItemStack energyStack = energySlot.getStack();

				if (MagitekMechs.trVersionNew){
					if (EnergyStorageUtil.isEnergyStorage(energyStack)) {
						long energy = 0;
						if (SimpleBatteryItem.class.isInstance(energyStack.getItem())) {
							SimpleBatteryItem sbi = (SimpleBatteryItem) energyStack.getItem();
							energy = sbi.getStoredEnergy(energyStack);
						}

						if (energy > 0) {
							this.propertyDelegate.set(3, 1);
						}
						else {
							this.propertyDelegate.set(3, 0); // Energy not in slot
							this.propertyDelegate.set(4, 0); // Reset power draw animation
						}
					}
					else {
						this.propertyDelegate.set(3, 0); // Energy not in slot
						this.propertyDelegate.set(4, 0); // Reset power draw animation
					}
				}
				else {
					if (Energy.valid(energyStack)) {

						double energy = Energy.of(energyStack).getEnergy();
						if (energy > 0) {
							this.propertyDelegate.set(3, 1);
						}
						else {
							this.propertyDelegate.set(3, 0); // Energy not in slot
							this.propertyDelegate.set(4, 0); // Reset power draw animation
						}
					}
					else {
						this.propertyDelegate.set(3, 0); // Energy not in slot
						this.propertyDelegate.set(4, 0); // Reset power draw animation
					}
				}



			}
			else {
				this.propertyDelegate.set(3, 0); // Energy not in slot
				this.propertyDelegate.set(4, 0); // Reset power draw animation
			}
		}
		return super.getSlot(index);
	}

	@Override
	public ItemStack transferSlot(PlayerEntity player, int invSlot) {
		
		ItemStack newStack = ItemStack.EMPTY;
		Slot slot = this.slots.get(invSlot);
		if (slot != null && slot.hasStack()) {
			ItemStack originalStack = slot.getStack();
			newStack = originalStack.copy();
			if (invSlot < this.inventory.size()) {
				if (!this.insertItem(originalStack, this.inventory.size(), this.slots.size(), true)) {
					return ItemStack.EMPTY;
				}
			} else if (!this.insertItem(originalStack, 0, this.inventory.size(), false)) {
				return ItemStack.EMPTY;
			}

			if (originalStack.isEmpty()) {
				slot.setStack(ItemStack.EMPTY);
			} else {
				slot.markDirty();
			}
		}
		return newStack;
	}

}