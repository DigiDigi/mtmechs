package digidigi.mtmechs.screen;

import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

public class MagitekArmorScreen extends HandledScreen<ScreenHandler>{
    private static final Identifier TEXTURE = new Identifier("mtmechs", "textures/gui/magui.png");
 
    public MagitekArmorScreen(ScreenHandler handler, PlayerInventory inventory, Text title) {
    	super(handler, inventory, title);
    }
 
    @Override
    protected void drawBackground(MatrixStack matrices, float delta, int mouseX, int mouseY) {
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.setShaderTexture(0, TEXTURE);
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        drawTexture(matrices, x, y, 0, 0, backgroundWidth, backgroundHeight);
        MagitekArmorScreenHandler mahandler = (MagitekArmorScreenHandler)this.handler;
        
    	int barY = (int)(mahandler.getSyncedEnergyStored());
    	drawTexture(matrices, x+27, (y+38)-barY, 192, 23, 10, barY);    // Overlay energy bar
        
        if (mahandler.getSyncedMagiciteSlotted() == 1) {
        	drawTexture(matrices, x+111, y+21, 177, 0, 22, 22);   // activation indicator
        }
        if (mahandler.getSyncedAddonSlotted() > 0) {
        	drawTexture(matrices, x+137, y+28, 177, 38, 14, 14);  // activation indicator
        	drawTexture(matrices, x+136, y+46, 200, 0, 16, 16);  // gray out slot background
        }
    }
 
    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        renderBackground(matrices);
        super.render(matrices, mouseX, mouseY, delta);
        drawMouseoverTooltip(matrices, mouseX, mouseY);
    }
 
    @Override
    protected void init() {
        super.init();
        // Center the title
        titleX = (backgroundWidth - textRenderer.getWidth(title)) / 2;
    }
}

