package digidigi.mtmechs.screen;

import com.mojang.blaze3d.systems.RenderSystem;

import digidigi.mtmechs.MagitekMechs;
import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

public class ProtoArmorScreen extends HandledScreen<ScreenHandler>{
    private static final Identifier TEXTURE = new Identifier("mtmechs", "textures/gui/pagui.png");

    public ProtoArmorScreen(ScreenHandler handler, PlayerInventory inventory, Text title) {
    	super(handler, inventory, title);
    }
 
    @Override
    protected void drawBackground(MatrixStack matrices, float delta, int mouseX, int mouseY) {
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.setShaderTexture(0, TEXTURE);
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        drawTexture(matrices, x, y, 0, 0, backgroundWidth, backgroundHeight);
    	ProtoArmorScreenHandler pahandler = (ProtoArmorScreenHandler)this.handler;
    	
    	// parameter note:
    	// (matrices, output gui x, output gui y, x from source texture, y from source texture, width, height)
        if (!MagitekMechs.trInstalled) {
        	if (pahandler.getSyncedMagiciteSlotted() == 1) {
        		drawTexture(matrices, x+35, y+46, 212, 17, 16, 16);   // Infinite Energy icon lit
        	}
        	else {
        		drawTexture(matrices, x+35, y+46, 229, 17, 16, 16);   // Infinite Energy icon unlit
        	}
        	
        }
        else {
        	drawTexture(matrices, x+35, y+46, 212, 34, 16, 16);   // Little energy icon
        }
        
    	int barY = (int)(pahandler.getSyncedEnergyStored());
    	drawTexture(matrices, x+15, (y+65)-barY, 200, 0, 11, barY);    // Overlay energy bar
        
        if (pahandler.getSyncedMagiciteSlotted() == 1) {
        	drawTexture(matrices, x+122, y+21, 177, 0, 22, 22);   // activation indicator
        	if (pahandler.getSyncedEnergySlotted() == 1) {
        		drawTexture(matrices, x+29, y+30, 177, 54 + (16*pahandler.getSyncedPowerDrawFrame()), 20, 15);  // power wire
        	}
        }
        if (pahandler.getSyncedAddonSlotted() > 0) {
        	drawTexture(matrices, x+148, y+27, 177, 38, 14, 14);  // activation indicator
        	drawTexture(matrices, x+147, y+46, 212, 0, 16, 16);  // gray out slot background
        }
    }
 
    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        renderBackground(matrices);
        super.render(matrices, mouseX, mouseY, delta);
        drawMouseoverTooltip(matrices, mouseX, mouseY);
    }
 
    @Override
    protected void init() {
        super.init();
        // Center the title
        titleX = (backgroundWidth - textRenderer.getWidth(title)) / 2;
    }
}

