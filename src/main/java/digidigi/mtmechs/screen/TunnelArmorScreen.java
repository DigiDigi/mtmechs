package digidigi.mtmechs.screen;

import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.gui.screen.ingame.HandledScreen;
import net.minecraft.client.render.GameRenderer;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.text.Text;
import net.minecraft.util.Identifier;

public class TunnelArmorScreen extends HandledScreen<ScreenHandler>{
    private static final Identifier TEXTURE = new Identifier("mtmechs", "textures/gui/tagui.png");
 
    public TunnelArmorScreen(ScreenHandler handler, PlayerInventory inventory, Text title) {
    	super(handler, inventory, title);
    }
 
    @Override
    protected void drawBackground(MatrixStack matrices, float delta, int mouseX, int mouseY) {
        RenderSystem.setShader(GameRenderer::getPositionTexShader);
        RenderSystem.setShaderColor(1.0F, 1.0F, 1.0F, 1.0F);
        RenderSystem.setShaderTexture(0, TEXTURE);
        int x = (width - backgroundWidth) / 2;
        int y = (height - backgroundHeight) / 2;
        drawTexture(matrices, x, y, 0, 0, backgroundWidth, backgroundHeight);
        
        TunnelArmorScreenHandler tahandler = (TunnelArmorScreenHandler)this.handler;
        
    	int barY = (int)(tahandler.getSyncedEnergyStored());
    	if (barY > 0) {
        	drawTexture(matrices, x+24, (y+38)-barY, 208, 23, 10, barY);    // Overlay fuel bar 1
        	drawTexture(matrices, x+46, (y+38)-barY, 208, 23, 10, barY);    // Overlay fuel bar 2
    		drawTexture(matrices, x+38, y+27, 190, 24, 4, 12);    // Fuel Indicator
    	}
        if (tahandler.getSyncedMagiciteSlotted() == 1) {
        	drawTexture(matrices, x+132, y+23, 177, 0, 22, 22);   // activation indicator
        }
        if (tahandler.fuelSlot1.hasStack()) {
        	drawTexture(matrices, x+21, y+46, 200, 0, 16, 16);
        }
        if (tahandler.fuelSlot2.hasStack()) {
        	drawTexture(matrices, x+43, y+46, 200, 0, 16, 16);
        }
        
    }
 
    @Override
    public void render(MatrixStack matrices, int mouseX, int mouseY, float delta) {
        renderBackground(matrices);
        
        super.render(matrices, mouseX, mouseY, delta);
        TunnelArmorScreenHandler tahandler = (TunnelArmorScreenHandler)this.handler;
        drawMouseoverTooltip(matrices, mouseX, mouseY);
    }
 
    @Override
    protected void init() {
        super.init();
        // Center the title
        titleX = (backgroundWidth - textRenderer.getWidth(title)) / 2;
    }
}

