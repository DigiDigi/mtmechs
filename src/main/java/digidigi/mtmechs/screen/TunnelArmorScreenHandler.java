package digidigi.mtmechs.screen;

import digidigi.mtmechs.MagitekMechs;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventory;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.screen.ArrayPropertyDelegate;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.screen.slot.Slot;
import net.minecraft.util.math.BlockPos;

public class TunnelArmorScreenHandler extends ScreenHandler {
	private final Inventory inventory;
	PropertyDelegate propertyDelegate;

	private BlockPos pos;
	
	public Slot fuelSlot1;
	public Slot fuelSlot2;
	
	public int fuelSlotIndex1 = 0;
	public int fuelSlotIndex2 = 1;
	public int magiciteSlotIndex = 2;
	
	public boolean magiciteSlotActive = false;

	public TunnelArmorScreenHandler(int syncId, PlayerInventory playerInventory, PacketByteBuf buf) {
		this(syncId, playerInventory, new SimpleInventory(12), new ArrayPropertyDelegate(5));
	}

	public TunnelArmorScreenHandler(int syncId, PlayerInventory playerInventory, Inventory inventory, PropertyDelegate propertyDelegate) {
		super(MagitekMechs.TA_SCREEN_HANDLER, syncId);
		checkSize(inventory, 12);
		this.inventory = inventory;
		this.propertyDelegate = propertyDelegate;
		this.addProperties(propertyDelegate);
		inventory.onOpen(playerInventory.player);

		int m;
		int l;

		// Fuel Slots
		this.fuelSlot1 = new Slot(inventory, 9, 21, 46);
		this.addSlot(this.fuelSlot1);
		
		this.fuelSlot2 = new Slot(inventory, 10, 43, 46);
		this.addSlot(this.fuelSlot2);

		// Core Slot
		this.addSlot(new Slot(inventory, 11, 135, 46));

		// Storage
		for (m = 0; m < 3; ++m) {
			for (l = 0; l < 3; ++l) {
				this.addSlot(new Slot(inventory, l + m * 3, 71 + l * 18, 18 + m * 18));
			}
		}

		//The player inventory
		for (m = 0; m < 3; ++m) {
			for (l = 0; l < 9; ++l) {
				this.addSlot(new Slot(playerInventory, l + m * 9 + 9, 8 + l * 18, 84 + m * 18));
			}
		}
		//The player Hotbar
		for (m = 0; m < 9; ++m) {
			this.addSlot(new Slot(playerInventory, m, 8 + m * 18, 142));
		}

	}
	
	public int getSyncedMagiciteSlotted() {
		return propertyDelegate.get(0);
	}
	public int getSyncedEnergyStored() {
		return propertyDelegate.get(2);
	}
	public int getSyncedFuelSlotted() {
		return propertyDelegate.get(5);
	}

	public BlockPos getPos() {
		return pos;
	}

	@Override
	public boolean canUse(PlayerEntity player) {
		return this.inventory.canPlayerUse(player);
	}

	@Override
	public Slot getSlot(int index) {
		if (index == this.magiciteSlotIndex) {
			this.magiciteSlotActive = false;
			this.propertyDelegate.set(0, 0);
			Slot magiciteSlot = this.slots.get(this.magiciteSlotIndex);
			
			if (magiciteSlot != null && magiciteSlot.hasStack()) {
				ItemStack magiciteStack = magiciteSlot.getStack();
				
				if (magiciteStack.getItem() == MagitekMechs.MAGICITE_ITEM) {
					this.magiciteSlotActive = true;
					this.propertyDelegate.set(0, 1); // Magicite in slot.
				}
			}
		}
		
		return super.getSlot(index);
	}
	
	@Override
	public ItemStack transferSlot(PlayerEntity player, int invSlot) {
		ItemStack newStack = ItemStack.EMPTY;
		Slot slot = this.slots.get(invSlot);
		if (slot != null && slot.hasStack()) {
			ItemStack originalStack = slot.getStack();
			newStack = originalStack.copy();
			if (invSlot < this.inventory.size()) {
				if (!this.insertItem(originalStack, this.inventory.size(), this.slots.size(), true)) {
					return ItemStack.EMPTY;
				}
			} else if (!this.insertItem(originalStack, 0, this.inventory.size(), false)) {
				return ItemStack.EMPTY;
			}

			if (originalStack.isEmpty()) {
				slot.setStack(ItemStack.EMPTY);
			} else {
				slot.markDirty();
			}
		}
		return newStack;
	}

}