package digidigi.mtmechs;

import java.util.HashSet;
import java.util.Set;

import digidigi.mtmechs.compat.FlanCompat;
import digidigi.mtmechs.compat.GOMLCompat;
import digidigi.mtmechs.entity.MagitekArmorEntity;
import digidigi.mtmechs.entity.ProtoArmorEntity;
import digidigi.mtmechs.entity.TunnelArmorEntity;
import digidigi.mtmechs.item.WholeItem;
import digidigi.mtmechs.screen.MagitekArmorScreenHandler;
import digidigi.mtmechs.screen.ProtoArmorScreenHandler;
import digidigi.mtmechs.screen.TunnelArmorScreenHandler;
import net.fabricmc.api.ModInitializer;
import net.fabricmc.fabric.api.client.itemgroup.FabricItemGroupBuilder;
import net.fabricmc.fabric.api.gamerule.v1.CustomGameRuleCategory;
import net.fabricmc.fabric.api.gamerule.v1.GameRuleFactory;
import net.fabricmc.fabric.api.gamerule.v1.GameRuleRegistry;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.loot.v1.event.LootTableLoadingCallback;
import net.fabricmc.fabric.api.networking.v1.ServerPlayConnectionEvents;
import net.fabricmc.fabric.api.networking.v1.ServerPlayNetworking;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricDefaultAttributeRegistry;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.fabricmc.fabric.api.screenhandler.v1.ScreenHandlerRegistry;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.sound.SoundEvent;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.GameRules;
import net.minecraft.world.GameRules.Category;
import software.bernie.geckolib3.GeckoLib;

public class MagitekMechs implements ModInitializer {

    public static final Identifier MECHSTEP = new Identifier("mtmechs", "mechstep");
    public static final Identifier MECHSTEPTWO = new Identifier("mtmechs", "mechsteptwo");
//    public static final Identifier ROLLSTART = new Identifier("mtmechs", "rollstart");
//    public static final Identifier ROLLCONTINUE = new Identifier("mtmechs", "rollcontinue");
    public static final Identifier GRIND_LOOP = new Identifier("mtmechs", "grind_loop");
    public static final Identifier DRILL_LOOP = new Identifier("mtmechs", "drill_loop");
    public static final Identifier FIRE_BEAM = new Identifier("mtmechs", "firebeam");
    public static final Identifier OPENING = new Identifier("mtmechs", "opening");

    public static final ItemGroup MTM_GROUP = FabricItemGroupBuilder.create(
                    new Identifier("mtmechs", "itemsgroup"))
            .icon(() -> MagitekMechs.MA_WHOLE_ITEM.getDefaultStack())
            .build();

    public static SoundEvent mechstep = new SoundEvent(MECHSTEP);
    public static SoundEvent mechsteptwo = new SoundEvent(MECHSTEPTWO);
//    public static SoundEvent rollstart = new SoundEvent(ROLLSTART);
//    public static SoundEvent rollcontinue = new SoundEvent(ROLLCONTINUE);
    public static SoundEvent grind_loop = new SoundEvent(GRIND_LOOP);
    public static SoundEvent drill_loop = new SoundEvent(DRILL_LOOP);
    public static SoundEvent firebeam = new SoundEvent(FIRE_BEAM);
    public static SoundEvent opening = new SoundEvent(OPENING);

    public static final EntityType<TunnelArmorEntity> TUNNELARMOR = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier("mtmechs", "tunnelarmor"),
            FabricEntityTypeBuilder.create(SpawnGroup.CREATURE, TunnelArmorEntity::new).dimensions(EntityDimensions.fixed(2.5f, 2.00f)).build()
    );
    public static final EntityType<MagitekArmorEntity> MAGITEKARMOR = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier("mtmechs", "magitekarmor"),
            FabricEntityTypeBuilder.create(SpawnGroup.CREATURE, MagitekArmorEntity::new).dimensions(EntityDimensions.fixed(1.25f, 2.00f)).build()

    );
    public static final EntityType<ProtoArmorEntity> PROTOARMOR = Registry.register(
            Registry.ENTITY_TYPE,
            new Identifier("mtmechs", "protoarmor"),
            FabricEntityTypeBuilder.create(SpawnGroup.CREATURE, ProtoArmorEntity::new).dimensions(EntityDimensions.fixed(1.25f, 2.00f)).build()

    );

    public static final CustomGameRuleCategory MTMECHSGAMERULES = new CustomGameRuleCategory(new Identifier("mtmechs", "mtmechsrules"),
            new TranslatableText("gamerule.category.mtmechs").formatted(Formatting.BOLD).formatted(Formatting.YELLOW));

    public static final GameRules.Key<GameRules.BooleanRule> BEAM_DAMAGE = GameRuleRegistry.register("beamDamage", MTMECHSGAMERULES, GameRuleFactory.createBooleanRule(true));
    public static final GameRules.Key<GameRules.BooleanRule> BEAM_FIRE = GameRuleRegistry.register("beamFire", MTMECHSGAMERULES, GameRuleFactory.createBooleanRule(true));
    public static final GameRules.Key<GameRules.BooleanRule> BEAM_ICE = GameRuleRegistry.register("beamIce", MTMECHSGAMERULES, GameRuleFactory.createBooleanRule(true));
    public static final GameRules.Key<GameRules.BooleanRule> MECH_OWNERSHIP = GameRuleRegistry.register("mechOwnership", MTMECHSGAMERULES, GameRuleFactory.createBooleanRule(false));

    public static final Item MA_WHOLE_ITEM = new WholeItem(MAGITEKARMOR, new FabricItemSettings().group(MTM_GROUP));
    public static final Item TA_WHOLE_ITEM = new WholeItem(TUNNELARMOR, new FabricItemSettings().group(MTM_GROUP));
    public static final Item PA_WHOLE_ITEM = new WholeItem(PROTOARMOR, new FabricItemSettings().group(MTM_GROUP));
    public static final Item MAGICITE_ITEM = new Item(new FabricItemSettings().group(MTM_GROUP));
    public static final Item MA_CHASSIS_ITEM = new Item(new FabricItemSettings().group(MTM_GROUP));
    public static final Item MA_ARM_ITEM = new Item(new FabricItemSettings().group(MTM_GROUP));
    public static final Item MA_LEG_ITEM = new Item(new FabricItemSettings().group(MTM_GROUP));
    public static final Item MA_STACK_ITEM = new Item(new FabricItemSettings().group(MTM_GROUP));
    public static final Item PA_CHASSIS_ITEM = new Item(new FabricItemSettings().group(MTM_GROUP));
    public static final Item PA_LEG_ITEM = new Item(new FabricItemSettings().group(MTM_GROUP));
    public static final Item TA_CHASSIS_ITEM = new Item(new FabricItemSettings().group(MTM_GROUP));
    public static final Item TA_ARM_ITEM = new Item(new FabricItemSettings().group(MTM_GROUP));
    public static final Item TA_WHEEL_ITEM = new Item(new FabricItemSettings().group(MTM_GROUP));
    public static final Item TA_STACK_ITEM = new Item(new FabricItemSettings().group(MTM_GROUP));
    public static final Item GEAR_ITEM = new Item(new FabricItemSettings().group(MTM_GROUP));
    public static final Identifier MACONTAINER = new Identifier("mtmechs", "ma_container");
    public static final Identifier PACONTAINER = new Identifier("mtmechs", "pa_container");
    public static final Identifier TACONTAINER = new Identifier("mtmechs", "ta_container");
    public static final ScreenHandlerType<MagitekArmorScreenHandler> MA_SCREEN_HANDLER;
    public static final ScreenHandlerType<ProtoArmorScreenHandler> PA_SCREEN_HANDLER;
    public static final ScreenHandlerType<TunnelArmorScreenHandler> TA_SCREEN_HANDLER;

    static {
        MA_SCREEN_HANDLER = ScreenHandlerRegistry.registerExtended(MACONTAINER, MagitekArmorScreenHandler::new);
        PA_SCREEN_HANDLER = ScreenHandlerRegistry.registerExtended(PACONTAINER, ProtoArmorScreenHandler::new);
        TA_SCREEN_HANDLER = ScreenHandlerRegistry.registerExtended(TACONTAINER, TunnelArmorScreenHandler::new);
    }

    public static final Set<String> attachedMods = new HashSet<>();
    public static final Set<Identifier> lootTables = new HashSet<>();

    public static boolean trInstalled = false;
    public static boolean trVersionNew = false;

    @Override
    public void onInitialize() {
        // This code runs as soon as Minecraft is in a mod-load-ready state.
        // However, some things (like resources) may still be uninitialized.
        // Proceed with mild caution.
        Registry.register(Registry.ITEM, new Identifier("mtmechs", "magicite_item"), MAGICITE_ITEM);
        Registry.register(Registry.ITEM, new Identifier("mtmechs", "ma_whole_item"), MA_WHOLE_ITEM);
        Registry.register(Registry.ITEM, new Identifier("mtmechs", "ma_chassis_item"), MA_CHASSIS_ITEM);
        Registry.register(Registry.ITEM, new Identifier("mtmechs", "ma_arm_item"), MA_ARM_ITEM);
        Registry.register(Registry.ITEM, new Identifier("mtmechs", "ma_leg_item"), MA_LEG_ITEM);
        Registry.register(Registry.ITEM, new Identifier("mtmechs", "ma_stack_item"), MA_STACK_ITEM);
        Registry.register(Registry.ITEM, new Identifier("mtmechs", "pa_whole_item"), PA_WHOLE_ITEM);
        Registry.register(Registry.ITEM, new Identifier("mtmechs", "pa_chassis_item"), PA_CHASSIS_ITEM);
        Registry.register(Registry.ITEM, new Identifier("mtmechs", "pa_leg_item"), PA_LEG_ITEM);
        Registry.register(Registry.ITEM, new Identifier("mtmechs", "ta_whole_item"), TA_WHOLE_ITEM);
        Registry.register(Registry.ITEM, new Identifier("mtmechs", "ta_chassis_item"), TA_CHASSIS_ITEM);
        Registry.register(Registry.ITEM, new Identifier("mtmechs", "ta_arm_item"), TA_ARM_ITEM);
        Registry.register(Registry.ITEM, new Identifier("mtmechs", "ta_wheel_item"), TA_WHEEL_ITEM);
        Registry.register(Registry.ITEM, new Identifier("mtmechs", "ta_stack_item"), TA_STACK_ITEM);
        Registry.register(Registry.ITEM, new Identifier("mtmechs", "iron_gear_item"), GEAR_ITEM);
        Registry.register(Registry.SOUND_EVENT, MECHSTEP, mechstep);
        Registry.register(Registry.SOUND_EVENT, MECHSTEPTWO, mechsteptwo);
//        Registry.register(Registry.SOUND_EVENT, ROLLSTART, rollstart);
//        Registry.register(Registry.SOUND_EVENT, ROLLCONTINUE, rollcontinue);
        Registry.register(Registry.SOUND_EVENT, GRIND_LOOP, grind_loop);
        Registry.register(Registry.SOUND_EVENT, DRILL_LOOP, drill_loop);
        Registry.register(Registry.SOUND_EVENT, FIRE_BEAM, firebeam);
        GeckoLib.initialize();
        FabricDefaultAttributeRegistry.register(MAGITEKARMOR, MagitekArmorEntity.createMobAttributes());
        FabricDefaultAttributeRegistry.register(PROTOARMOR, ProtoArmorEntity.createMobAttributes());
        FabricDefaultAttributeRegistry.register(TUNNELARMOR, TunnelArmorEntity.createMobAttributes());

        ServerPlayConnectionEvents.INIT.register((handler, server) -> {
            ServerPlayNetworking.registerReceiver(handler, NetIdentifiers.ta_p_to_server, TunnelArmorEntity::receiveDrillToggleServer);
        });

        ServerPlayConnectionEvents.INIT.register((handler, server) -> {
            ServerPlayNetworking.registerReceiver(handler, NetIdentifiers.ma_p_to_server, MagitekArmorEntity::receiveBeamServer);
        });

        ServerPlayConnectionEvents.INIT.register((handler, server) -> {
            ServerPlayNetworking.registerReceiver(handler, NetIdentifiers.pa_p_to_server, ProtoArmorEntity::receiveBeamServer);
        });

        lootTables.add(new Identifier("minecraft:chests/abandoned_mineshaft"));
        lootTables.add(new Identifier("minecraft:chests/bastion_bridge"));
        lootTables.add(new Identifier("minecraft:chests/bastion_hoglin_stable"));
        lootTables.add(new Identifier("minecraft:chests/bastion_other"));
        lootTables.add(new Identifier("minecraft:chests/buried_treasure"));
        lootTables.add(new Identifier("minecraft:chests/desert_pyramid"));
        lootTables.add(new Identifier("minecraft:chests/end_city_treasure"));
        lootTables.add(new Identifier("minecraft:chests/igloo_chest"));
        lootTables.add(new Identifier("minecraft:chests/jungle_temple"));
        lootTables.add(new Identifier("minecraft:chests/nether_bridge"));
        lootTables.add(new Identifier("minecraft:chests/pillager_outpost"));
        lootTables.add(new Identifier("minecraft:chests/ruined_portal"));
        lootTables.add(new Identifier("minecraft:chests/shipwreck_treasure"));
        lootTables.add(new Identifier("minecraft:chests/simple_dungeon"));
        lootTables.add(new Identifier("minecraft:chests/stronghold_corridor"));
        lootTables.add(new Identifier("minecraft:chests/stronghold_crossing"));
        lootTables.add(new Identifier("minecraft:chests/stronghold_library"));
        lootTables.add(new Identifier("minecraft:chests/underwater_ruin_big"));
        lootTables.add(new Identifier("minecraft:chests/underwater_ruin_small"));
        lootTables.add(new Identifier("minecraft:chests/woodland_mansion"));
        lootTables.add(new Identifier("minecraft:gameplay/fishing/treasure"));

        if (FabricLoader.getInstance().isModLoaded("techreborn")) {
            attachedMods.add("techreborn");

            trInstalled = true;
            String trVersion = FabricLoader.getInstance().getModContainer("techreborn").get().getMetadata().getVersion().getFriendlyString();
            String[] trVersionSplits = trVersion.split("\\.");

            // this version didn't parse correctly because a word was in front of it.
            // project.5.0.7-beta+build.107
            int versionIndex = 0;
            if (trVersionSplits[0].equalsIgnoreCase("project")){
                versionIndex = 1;
            }

            int trVFirstElement = Integer.parseInt(trVersionSplits[0+versionIndex]);
            int trVSecondElement = Integer.parseInt(trVersionSplits[1+versionIndex]);
            int trVThirdElement = Integer.parseInt(trVersionSplits[2+versionIndex].split("\\-")[0]);
            if (trVFirstElement > 5 || trVSecondElement > 0 || trVThirdElement >= 7) {
                trVersionNew = true;
            }
        }
        else {
            trInstalled = false;
        }

        if (FabricLoader.getInstance().isModLoaded("flan")) {
            attachedMods.add("flan");
            FlanCompat.installed = true;
        }
        if (FabricLoader.getInstance().isModLoaded("goml")) {
            attachedMods.add("goml");
            GOMLCompat.installed = true;
        }

        // Quick command for testing loot tables:
        // /setblock ~ ~ ~ chest{LootTable:"chests/simple_dungeon"} replace
        LootTableLoadingCallback.EVENT.register((resourceManager, lootManager, id, table, setter) -> {
            if (lootTables.contains(id)) {
//		        FabricLootPoolBuilder poolBuilder = FabricLootPoolBuilder.builder()
//		                .rolls(ConstantLootNumberProvider.create(1))
//		                .conditionally(RandomChanceLootCondition.builder(0.1111f)) // ~1 in 9
//		                .with(ItemEntry.builder(PA_WHOLE_ITEM));
//		        table.pool(poolBuilder);
//		        FabricLootPoolBuilder poolBuilder2 = FabricLootPoolBuilder.builder()
//		                .rolls(ConstantLootNumberProvider.create(1))
//		                .conditionally(RandomChanceLootCondition.builder(0.1111f)) // ~1 in 9
//		                .with(ItemEntry.builder(MAGICITE_ITEM));
//		        table.pool(poolBuilder2);

            }
        });
    }
}
