package digidigi.mtmechs;

import org.lwjgl.glfw.GLFW;

import net.fabricmc.fabric.api.client.keybinding.v1.KeyBindingHelper;
import net.minecraft.client.option.KeyBinding;
import net.minecraft.client.util.InputUtil;

public class KeyBindings {
	
	public static KeyBinding mtkDebug;
	public static KeyBinding mtkPrimary;
	
	public static void KeyBindsInit() {
//	    mtkDebug = KeyBindingHelper.registerKeyBinding(new KeyBinding(
//	            "key.mtmechs.debug", // The translation key of the keybinding's name
//	            InputUtil.Type.KEYSYM, // The type of the keybinding, KEYSYM for keyboard, MOUSE for mouse.
//	            GLFW.GLFW_KEY_R, // The keycode of the key
//	            "category.mtmechs.debug" // The translation key of the keybinding's category.
//	        ));
	    mtkPrimary = KeyBindingHelper.registerKeyBinding(new KeyBinding(
	            "key.mtmechs.primary", // The translation key of the keybinding's name
	            InputUtil.Type.KEYSYM, // The type of the keybinding, KEYSYM for keyboard, MOUSE for mouse.
	            GLFW.GLFW_KEY_V, // The keycode of the key
	            "category.mtmechs.vehicle" // The translation key of the keybinding's category.
	        ));
	    
	}

}
