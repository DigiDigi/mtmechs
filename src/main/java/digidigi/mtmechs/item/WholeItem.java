package digidigi.mtmechs.item;

import digidigi.mtmechs.entity.MagitekArmorEntity;
import digidigi.mtmechs.entity.ProtoArmorEntity;
import digidigi.mtmechs.entity.TunnelArmorEntity;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnReason;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemUsageContext;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;
import net.minecraft.world.event.GameEvent;
import org.jetbrains.annotations.Nullable;

import java.util.Objects;

public class WholeItem extends Item {
    private final EntityType<?> type;

    public WholeItem(EntityType<? extends MobEntity> MagitekType, Settings settings) {
        super(settings);
        this.type = MagitekType;
    }

    public EntityType<?> getEntityType(@Nullable NbtCompound nbt) {
        if (nbt != null && nbt.contains("EntityTag", 10)) {
            NbtCompound nbtCompound = nbt.getCompound("EntityTag");
            if (nbtCompound.contains("id", 8)) {
                return (EntityType)EntityType.get(nbtCompound.getString("id")).orElse(this.type);
            }
        }
        return this.type;
    }

    @Override
    public ActionResult useOnBlock(ItemUsageContext context) {
        World world = context.getWorld();
        if (!(world instanceof ServerWorld)) {
            return ActionResult.SUCCESS;
        } else {
            ItemStack itemStack = context.getStack();
            BlockPos blockPos = context.getBlockPos();
            Direction direction = context.getSide();
            BlockState blockState = world.getBlockState(blockPos);

            BlockPos blockPos3;
            if (blockState.getCollisionShape(world, blockPos).isEmpty()) {
                blockPos3 = blockPos;
            } else {
                blockPos3 = blockPos.offset(direction);
            }

            EntityType<?> entityType2 = this.getEntityType(itemStack.getNbt());
            Entity spawnedEntity = this.spawnFromItemStack(entityType2, (ServerWorld)world, itemStack, context.getPlayer(), blockPos3, SpawnReason.SPAWN_EGG, true, !Objects.equals(blockPos, blockPos3) && direction == Direction.UP);
            if (spawnedEntity != null) {
                itemStack.decrement(1);
                world.emitGameEvent(context.getPlayer(), GameEvent.ENTITY_PLACE, blockPos);
            }

            return ActionResult.CONSUME;
        }
    }

    @Nullable
    public Entity spawnFromItemStack(EntityType entityType, ServerWorld world, @Nullable ItemStack stack, @Nullable PlayerEntity player, BlockPos pos, SpawnReason spawnReason, boolean alignPosition, boolean invertY) {
        Entity spawnedEntity = entityType.spawn(world, stack == null ? null : stack.getNbt(), stack != null && stack.hasCustomName() ? stack.getName() : null, player, pos, spawnReason, alignPosition, invertY);

        if (spawnedEntity != null) {
            if (spawnedEntity instanceof TunnelArmorEntity) {
                ((TunnelArmorEntity) spawnedEntity).writeOwner(player.getUuid());
            }
            else if (spawnedEntity instanceof MagitekArmorEntity) {
                ((MagitekArmorEntity) spawnedEntity).writeOwner(player.getUuid());
            }
            else if (spawnedEntity instanceof ProtoArmorEntity) {
                ((ProtoArmorEntity) spawnedEntity).writeOwner(player.getUuid());
            }
        }
        return spawnedEntity;
    }
}