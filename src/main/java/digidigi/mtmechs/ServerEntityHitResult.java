package digidigi.mtmechs;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

import com.google.common.collect.Lists;
import net.minecraft.entity.boss.dragon.EnderDragonEntity;
import net.minecraft.entity.boss.dragon.EnderDragonPart;
import net.minecraft.world.entity.EntityLookup;
import org.jetbrains.annotations.Nullable;

import net.minecraft.entity.Entity;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

public class ServerEntityHitResult extends HitResult {
	private final Entity entity;

	public ServerEntityHitResult(Entity entity, Vec3d vec3d) {
		super(vec3d);
		this.entity = entity;
	}

    @Nullable
    public static EntityHitResult raycast(Entity entity, Vec3d vec3d, Vec3d vec3d2, Box box, Predicate<Entity> predicate, double d) {
        World world = entity.world;
        double e = d;
        Entity entity2 = null;
        Vec3d vec3d3 = null;
        Iterator var12 = world.getOtherEntities(entity, box, predicate).iterator();
        //Iterator var12 = ServerEntityHitResult.customGetOtherEntities(world, entity, entity2, box).iterator();

            while(var12.hasNext()) {
                Entity entity3 = (Entity)var12.next();
                Box box2 = entity3.getBoundingBox().expand((double)entity3.getTargetingMargin());
                Optional<Vec3d> optional = box2.raycast(vec3d, vec3d2);
                if (box2.contains(vec3d)) {
                    if (e >= 0.0D) {
                        entity2 = entity3;
                        vec3d3 = (Vec3d)optional.orElse(vec3d);
                        e = 0.0D;
                    }
                } else if (optional.isPresent()) {
                    Vec3d vec3d4 = (Vec3d)optional.get();
                    double f = vec3d.squaredDistanceTo(vec3d4);
                    if (f < e || e == 0.0D) {
                        if (entity3.getRootVehicle() == entity.getRootVehicle()) {
                            if (e == 0.0D) {
                                entity2 = entity3;
                                vec3d3 = vec3d4;
                            }
                        } else {
                            entity2 = entity3;
                            vec3d3 = vec3d4;
                            e = f;
                        }
                    }
                }
            }

            if (entity2 == null) {
                return null;
            }

            return new EntityHitResult(entity2, vec3d3);
    }

//    private static List<Entity> customGetOtherEntities(@Nullable World world, Entity except, Entity except2, Box box) {
//        world.getProfiler().visit("getEntities");
//        List<Entity> list = Lists.newArrayList();
//
//        world.getEntityLookup().forEachIntersects(box, (entity2) -> {
//            if (entity2 != except && entity2 != except2) {
//                list.add(entity2);
//            }
//            if (entity2 instanceof EnderDragonEntity) {
//                EnderDragonPart[] var4 = ((EnderDragonEntity)entity2).getBodyParts();
//                int var5 = var4.length;
//
//                for(int var6 = 0; var6 < var5; ++var6) {
//                    EnderDragonPart enderDragonPart = var4[var6];
//                    if (entity2 != except && entity2 != enderDragonPart) {
//                        list.add(enderDragonPart);
//                    }
//                }
//            }
//
//        });
//        return list;
//    }

	public Entity getEntity() {
		return this.entity;
	}

	public HitResult.Type getType() {
		return HitResult.Type.ENTITY;
	}
}