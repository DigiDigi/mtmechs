# Magitek Mechs (1.0.10)

### Changelog
(1.0.10)
* claim mod fixes
* small typo

(1.0.9)
* reduced drill whirr volume

(1.0.8)
* added gravity
* sound adjustments

(1.0.7)
* decreased the TunnelArmor's bounding box to make it a bit easier to tunnel
* being embedded in gravel/sand will no longer damage TunnelArmors
* fixed wrench replacement when MechOwnership is off. 

(1.0.6)
* fixed some game rule formatting.
* mechs should no longer be spawn eggs

(1.0.5)
* goml compatibility
* stop beams from damaging through walls

(1.0.4)
* using a TechReborn wrench can now turn a mech back into an item
* fixed mob/player/terrain damage on (flan) claims
* fixed mechs being used in spawners (for diamonds)
* added basic ownership (/gamerule mechOwnership)
* tentatively removing ProtoArmor and Magicite from loot tables