Magitek Mechs 1.6.4+
=============

A minecraft mod introducing my take on Magitek Armor, a type of magic-infused power armor that appeared in Square's Final Fantasy 6. This is an entirely unofficial fan work!

Currently included:

* A walking rideable Magitek and Proto Armor.
* A mostly functional heat beam. Burns stuff. Smelts stuff.
* Working multiplayer.